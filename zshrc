# Colours.
autoload -U colors && colors

# Prompt.
# Exit code:
# %F{green}[]
prompt="%F{yellow}%?%f:%F{blue}%B%2~%b%f %# "
autoload -Uz promptinit
promptinit

# (Lines configured by zsh-newuser-install)
# History file.
HISTFILE=~/.config/zshhistfile
HISTSIZE=1000
SAVEHIST=1000000
HISTORY_IGNORE=" *"
setopt appendhistory autocd
# Vi mode.
bindkey -e
# bindkey -v
# bindkey -v '^?' backward-delete-char
# bindkey -v '^W' backward-kill-word
# bindkey "^R" history-incremental-search-backward
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'

# Autocompletion.
autoload -Uz compinit
autoload -Uz compaudit
compaudit

# Completions.
zstyle ':completion:*' menu select
zstyle ':completion:*' verbose yes
compinit
_comp_options+=(globdots)

fpath=(~/my-zsh-prompts $fpath)

# Filetype highlighting.
zmodload zsh/complist
#LS_COLORS=$(cat .dir_colors)
#zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
[ -f ~/.config/dir_colors ] && eval $(dircolors -b ~/.config/dir_colors)


# Plugins.
source ~/.guix-home/profile/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Config files.
source /etc/profile
source ~/.config/profile
source ~/.config/bash_aliases
#pfetch
#fortune -s

# pip zsh completion start
function _pip_completion {
  local words cword
  read -Ac words
  read -cn cword
  reply=( $( COMP_WORDS="$words[*]" \
             COMP_CWORD=$(( cword-1 )) \
             PIP_AUTO_COMPLETE=1 $words[1] 2>/dev/null ))
}
compctl -K _pip_completion pip

if [[ ! $DISPLAY && $XDG_VTNR -eq 1 && "$(tty)" = "/dev/tty1" ]]; then
    exec startx
fi
# pip zsh completion end
autoload edit-command-line; zle -N edit-command-line
bindkey -M vicmd v edit-command-line
bindkey -M emacs "^v" edit-command-line
