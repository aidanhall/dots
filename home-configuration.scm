;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu packages base)
             (gnu services)
             (guix gexp)
             (guix channels)
             (gnu home services desktop)
             (gnu home services shells)
             (gnu home services pm)
             (gnu home services syncthing)
             (gnu home services dotfiles)
             (gnu home services guix))

(define my-glibc-locales
  (make-glibc-utf8-locales
   glibc
   #:locales (list "en_GB")
   #:name "glibc-british-utf8-locale"))

(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
 (packages (specifications->packages (list "git"
                                           "nss-certs"
                                           "emacs-pgtk"
                                           "neofetch"
                                           "password-store"
                                           "glibc-locales"
                                           "guile-ssh"
                                           "direnv"
                                           "ripgrep"
                                           "man-db"
                                           "pandoc"
                                           "aspell"
                                           "aspell-dict-en"
                                           "neovim"
                                           "vim-full")))

 ;; Below is the list of Home services.  To search for available
 ;; services, run 'guix home search KEYWORD' in a terminal.
 (services
  (list (service home-bash-service-type
                 (home-bash-configuration
                  (aliases '(("grep" . "grep --color=auto") ("ll" . "ls -l")
                             ("ls" . "ls -p --color=auto")))
                  (bashrc (list (local-file
                                 ".bashrc"
                                 "bashrc")))))
        (service home-zsh-service-type
                 (home-zsh-configuration
                  (zshrc (list (local-file "zshrc" "zshrc")))))
        (service home-syncthing-service-type)
        (service home-dotfiles-service-type
                 (home-dotfiles-configuration
                  (layout 'stow)
                  (directories '("dotfiles"))))
        (simple-service
         'nonguix-packages-service
         home-channels-service-type
         (list (channel
                (name 'nonguix)
                (url "https://gitlab.com/nonguix/nonguix")
                ;; Enable signature verification:
                (introduction
                 (make-channel-introduction
                  "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                  (openpgp-fingerprint
                   "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))))
        (simple-service
         'guix-gaming-games-services
         home-channels-service-type
         (list (channel
                (name 'guix-gaming-games)
                (url "https://gitlab.com/guix-gaming-channels/games.git")
                ;; Enable signature verification:
                (introduction
                 (make-channel-introduction
                  "c23d64f1b8cc086659f8781b27ab6c7314c5cca5"
                  (openpgp-fingerprint
                   "50F3 3E2E 5B0C 3D90 0424  ABE8 9BDC F497 A4BB CC7F")))))))))
