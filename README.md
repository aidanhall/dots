# Dots
I didn't call this dotfiles because I already have a dotfiles repo.

## Stow
It's memed a lot but it seems like a good way of managing lots of files, and might save me time after using it for a while.

## Usage
Run:
```shell
$ cd dots
$ ln -s .bashrc ~
$ ln -s zshrc ~/.zshrc
$ cd dotfiles
$ stow -v -t ~ */
```
The glob `*/` matches every subdirectory of the current one.
This operates assuming that the files in each of these subdirectories should be symlinked to the home folder.
A verbosity level of 1 or 2 might be a good idea as well.
