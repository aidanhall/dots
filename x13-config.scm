;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu)
             (gnu packages shells)
             (gnu packages xorg)
             (gnu packages video)
             (gnu services pm)
             (gnu services desktop)
             (nongnu packages linux)
             (nongnu packages video)
             (guix packages)
             (gnu packages emacs-xyz)
             (guix utils))
(use-service-modules cups desktop networking ssh xorg sound)

(operating-system
 (locale "en_GB.utf8")
 (timezone "Europe/London")
 (keyboard-layout (keyboard-layout "gb"))
 (host-name "guix13")

 (kernel linux)
 (firmware (append (list ibt-hw-firmware iwlwifi-firmware sof-firmware i915-firmware)
                   %base-firmware))

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
                (name "aidan")
                (comment "Aidan Hall")
                (group "users")
                (home-directory "/home/aidan")
                (supplementary-groups '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))

 ;; Packages installed system-wide.  Users can also install packages
 ;; under their own account: use 'guix search KEYWORD' to search
 ;; for packages and 'guix install PACKAGE' to install a package.
 (packages (append
            (specifications->packages
             (list
	      "nss-certs"
              "iwlwifi-firmware"
              "font-google-noto"
              "font-google-noto-emoji"
              "font-google-noto-sans-cjk"
              "font-google-noto-serif-cjk"
              "libinput"
              "man-db"
              "gnupg"
              "icedove"
              "firefox"
              "tmux"
              "htop"))
            %base-packages))

 ;; Below is the list of system services.  To search for available
 ;; services, run 'guix system search KEYWORD' in a terminal.
 (services
  (modify-services
   (append (list

            ;; To configure OpenSSH, pass an 'openssh-configuration'
            ;; record as a second argument to 'service' below.
            (service openssh-service-type)
            (set-xorg-configuration
                  (xorg-configuration (keyboard-layout keyboard-layout)))
	    (service bluetooth-service-type)
            (service gnome-desktop-service-type))
           %desktop-services)
   (guix-service-type
    config =>
    (guix-configuration
     (inherit config)
     (substitute-urls
      (append %default-substitute-urls
              (list "https://substitutes.nonguix.org")))
     (authorized-keys
      (append %default-authorized-guix-keys
              (list (plain-file "non-guix.pub"
                                "(public-key
 (ecc
  (curve Ed25519)
  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
  )
 )")))))))
  )
 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))
 (swap-devices (list (swap-space
                      (target (uuid
                               "986ffbea-89ff-4279-8c6f-29b72d6f5e07")))))

 ;; The list of file systems that get "mounted".  The unique
 ;; file system identifiers there ("UUIDs") can be obtained
 ;; by running 'blkid' in a terminal.
 (file-systems (cons* (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "49D2-E118"
                                       'fat32))
                         (type "vfat"))
                       (file-system
                         (mount-point "/")
                         (device (uuid
                                  "8d879c86-58e5-4683-80e1-18a3a8934055"
                                  'ext4))
                         (type "ext4")) %base-file-systems)))
