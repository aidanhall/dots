" Configuration:
" This is mostly a tweak of the default colours, so we re-load them first.
syntax reset
hi clear
set background=dark

let g:colors_name = "aidan"

" Colours:
highlight User1 ctermbg=green ctermfg=red   guibg=green guifg=red
highlight User2 ctermbg=red   ctermfg=blue  guibg=red   guifg=blue
highlight User3 ctermbg=blue  ctermfg=green guibg=blue  guifg=green
highlight User4 ctermbg=lightblue ctermfg=black
highlight Comment ctermfg=cyan term=italic guifg=#80a0ff
highlight Constant ctermfg=magenta
highlight Identifier ctermfg=cyan
highlight Statement ctermfg=yellow guifg=yellow gui=none
highlight Type ctermfg=green guifg=lightgreen gui=none
highlight StorageClass guifg=#FFB224 ctermfg=darkyellow
highlight Error ctermbg=red ctermfg=black cterm=bold
highlight LineNr ctermfg=darkcyan guifg=Cyan gui=none
highlight CursorLineNr ctermfg=yellow guifg=Orange gui=none
highlight Search ctermbg=Yellow guibg=Yellow
highlight IncSearch ctermbg=lightyellow guibg=Orange guifg=Black gui=none
highlight Visual ctermbg=gray
highlight MatchParen ctermfg=black ctermbg=cyan
highlight lspReference ctermfg=black ctermbg=green
highlight Folded ctermfg=14 ctermbg=242 guifg=Cyan guibg=grey45
highlight ColorColumn ctermbg=darkgrey guibg=grey20
highlight Pmenu guibg=grey20 ctermbg=darkgrey

" Linting:
highlight SpellBad ctermfg=black ctermbg=darkred cterm=italic guibg=red guifg=black gui=undercurl
highlight SpellCap cterm=undercurl

" Terminal Colours: Force Defaults.
if has("nvim")
	unlet!
				\ g:terminal_color_0
				\ g:terminal_color_1
				\ g:terminal_color_2
				\ g:terminal_color_3
				\ g:terminal_color_4
				\ g:terminal_color_5
				\ g:terminal_color_6
				\ g:terminal_color_7
				\ g:terminal_color_8
				\ g:terminal_color_9
				\ g:terminal_color_10
				\ g:terminal_color_11
				\ g:terminal_color_12
				\ g:terminal_color_13
				\ g:terminal_color_14
				\ g:terminal_color_15
endif

" Vimwiki:
highlight VimwikiDelText gui=strikethrough ctermfg=13 guifg=#ffa0a0
" Python:
highlight pythonAttribute ctermfg=blue
" Cpp:
highlight cppSTLconstant ctermfg=darkgreen guifg=lightgreen
highlight cppParen ctermfg=green
" CSharp:
highlight csBraces ctermfg=lightmagenta
highlight csUserIdentifier ctermfg=lightgreen
" Plain Text:
autocmd FileType text setlocal noautoindent nocindent
" Groff:
highlight nroffRefer ctermfg=lightgreen
