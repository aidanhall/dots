" Status Line:
set laststatus=2 " Always on.
" Clear statusline
set statusline=
" Left triangle
" set statusline+=\ 
" FILETYPE
" set statusline+=%Y\ 
" [filetype]
set statusline+=%y\ 
" Truncate after file type, if necessary
set statusline+=%<
" File name. Relative if possible
set statusline+=%f
" Modification flag
set statusline+=%m
" Read-only flag
set statusline+=%r
" Preview window flag
set statusline+=%w

" Centered items (equal spacing of multiple %= is a NeoVim extension):
set statusline+=%=
" Selected character hex code
set statusline+=[0x\%02.2B]\ 
" Help flag
set statusline+=%h
" Git branch/status
if &loadplugins == 1
	set statusline+=%{FugitiveStatusline()}
endif

" Right-aligned items:
set statusline+=%=
" Scroll Offset
set statusline+=s%{GetScrolloff()}\ 
" " Time
" set statusline+=%{strftime('%H:%M')}\ 
" Position in file
set statusline+=%l/%L,\%03v\ 
" Editing Mode
set statusline+=%{mode(\"\ \")}\ 
" Buffer number
set statusline+=[b%n]
" Right triangle
" set statusline+=\ 


