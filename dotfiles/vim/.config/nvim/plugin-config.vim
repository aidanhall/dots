" Standard Distribution Packages:
packadd termdebug
packadd cfilter
packadd matchit

" NeoVim Plugins:

if has("nvim-0.5")
	packadd nvim-lspconfig
	packadd nvim-treesitter
endif

" Seemingly Necessary:
packadd vim-nix

" Generate Help Tags:
helptags ALL
