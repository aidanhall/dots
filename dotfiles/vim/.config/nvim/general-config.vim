" Basic Global Configuration:
set nocompatible
set path+=**
filetype plugin on
syntax on

let mapleader = " "
let maplocalleader = "\\"
set colorcolumn=100,80
set completeopt=menu,preview,longest,menuone
set dictionary+=/usr/share/dict/words
set encoding=utf-8
set hidden
set hlsearch incsearch
set modeline
set modelines=5
set mouse=a
set number relativenumber
set smartcase ignorecase
set title
set wildmenu
set wildmode=longest:full:lastused
set splitbelow splitright
" set scrolloff=3
set listchars=trail:-,nbsp:+,precedes:^,tab:\ \ \│
set list
set secure
set exrc

" Indentation Tabs And Spacing:
set autoindent
set cindent
set cinoptions+=j:0l1g0N-
set shiftwidth=4
set softtabstop=-1

" Text Formatting
set cpoptions+=J
set joinspaces
set formatoptions+=/

" Folds:
set foldmethod=indent
set foldlevel=3


" Specific File Configuration:
autocmd BufWrite dunstrc :!pkill dunst; dunst & disown
autocmd BufWrite sxhkdrc :!pkill sxhkd; sxhkd & disown


" Key Bindings:

" Browser Style Tab Navigation:
nnoremap <M-1> 1gt
nnoremap <M-2> 2gt
nnoremap <M-3> 3gt
nnoremap <M-4> 4gt
nnoremap <M-5> 5gt
nnoremap <M-6> 6gt
nnoremap <M-7> 7gt
nnoremap <M-8> 8gt
nnoremap <M-9> 9gt
nnoremap <M-0> :$tabnext<CR>

" Alt Gr Keys: OS-dependent?
nnoremap « <<
nnoremap » >>
nnoremap ¢ ciw
nnoremap © ciW
nnoremap ð diw
nnoremap Ð diW

" Convenient Keys:
nnoremap Y y$
nnoremap ¬ ^
inoremap  
nnoremap <C-P> :FZF<CR>
nnoremap <leader><leader> :Lex <CR>
nnoremap <leader>th :call <SID>SynStack()<CR>
nnoremap <leader>d :!dragon-drag-and-drop %<CR>

" F Keys:
" F1: Help (default).
nnoremap <F2> :term <CR>
tnoremap <F2> exit <CR>
map <F3> :nohlsearch <CR>
map <F4> :call ToggleScrolloff()<CR>
nnoremap <F5> :terminal ++open $HOME/scripts/compile1.sh %:p<enter>
nnoremap <F6> :terminal ++close $HOME/scripts/compile2.sh %:p<enter>
nnoremap <F7> :Make<CR>
nnoremap <F8> :lwindow<CR>
nnoremap <F9> :ccl<space>\|<space>lcl<CR>
nnoremap <F10> :wincmd =<CR>
" F11: Unused. Likely makes desktop environment terminal emulators fullscreen.
nnoremap <F12> :setlocal spell! <CR>

" Quick Fix And Location List Navigation Keys:
nnoremap <silent> <M-J> :lne<CR>
nnoremap <silent> <M-K> :lpr<CR>
nnoremap <silent> <M-L> :.ll<CR>
nnoremap <silent> <M-Q> :lw<CR>
nnoremap <silent> <M-j> :laf<CR>
nnoremap <silent> <M-k> :lbe<CR>
nnoremap <silent> <M-C-j> :cn<CR>
nnoremap <silent> <M-C-k> :cp<CR>
nnoremap <silent> <M-C-l> :.cc<CR>
nnoremap <silent> <M-C-q> :cw<CR>

" Split Navigation Keys:
nnoremap <C-J> :wincmd j<CR>
nnoremap <C-K> :wincmd k<CR>
nnoremap <C-L> :wincmd l<CR>
nnoremap <C-H> :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <M-o> :wincmd w<CR>
tnoremap <M-o> :wincmd w<CR>
nnoremap <M-q> :wincmd q<CR>
tnoremap <M-q> :wincmd q<CR>
nnoremap <M-i> :edit #<CR>
tnoremap <M-i> :edit #<CR>

" Terminal:
nnoremap <M-u> :terminal<CR>
" Plugins:
runtime plugin-config.vim

" Status Line:
runtime statusline.vim


" Syntax Highlighting:
runtime highlight-config.vim



" Netrw:
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_usetab=1
let g:netrw_winsize=25


" LSP and other NeoVim goodness:
if has("nvim-0.5")
	lua require'lspconfig'.clangd.setup{}
	lua require'lspconfig'.pylsp.setup{}
	lua require'lspconfig'.vimls.setup{}
	lua require'lspconfig'.texlab.setup{}
	lua require'lspconfig'.zls.setup{}
	lua require'lspconfig'.rls.setup{}
	lua require'lspconfig'.hls.setup{}

	runtime treesitter-config.vim
	autocmd FileType c,cpp,python,vim,tex,latex,zig,zir,rust,haskell call MyLspConfig()

endif

function! LspLocationList(timer)
	lua vim.diagnostic.setloclist({open = false})
endfunction

function! LspHover()
	" Only call if there are no floating windows so it doesn't jump inside, hopefully.
	" TODO: Make this reliable.
	if len(filter(nvim_list_wins(), {k,v->nvim_win_get_config(v).relative != ""})) == 0
		lua vim.lsp.buf.hover()
	endif
endfunction

function! MyLspConfig()
	setlocal omnifunc=v:lua.vim.lsp.omnifunc
	let b:SuperTabDefaultCompletionType = "<c-x><c-o>"
	nnoremap <buffer> <silent>  <cmd>lua vim.lsp.buf.definition()<CR>
	nnoremap <buffer> <silent> <leader>f <cmd>lua vim.lsp.buf.formatting()<CR>
	nnoremap <buffer> <silent> <leader>K    <cmd>call LspHover()<CR>
	nnoremap <buffer> <silent> gl    <cmd>lua vim.lsp.buf.implementation()<CR>
	nnoremap <buffer> <silent> <M-h> <cmd>lua vim.lsp.buf.signature_help()<CR>
	nnoremap <buffer> <silent> 1gD   <cmd>lua vim.lsp.buf.type_definition()<CR>
	nnoremap <buffer> <silent> gr    <cmd>lua vim.lsp.buf.references()<CR>
	nnoremap <buffer> <silent> gO    <cmd>lua vim.lsp.buf.document_symbol()<CR>
	nnoremap <buffer> <silent> gW    <cmd>lua vim.lsp.buf.workspace_symbol()<CR>
	nnoremap <buffer> <silent> ga    <cmd>lua vim.lsp.buf.code_action()<CR>
	nnoremap <buffer> <silent> <M-r> <cmd>lua vim.lsp.buf.rename()<CR>
	autocmd CursorHold,CursorHoldI,CompleteChanged <buffer> call LspHover()
	autocmd CompleteDone <buffer> call CloseFloats()
	autocmd BufEnter <buffer> setlocal completeopt+=noselect,menuone,noinsert
	autocmd BufLeave <buffer> setlocal completeopt-=noselect,menuone,noinsert
	" We need to give the LSP a moment to load.
	autocmd InsertLeave,CursorHold <buffer> call LspLocationList(69) " 69 is just for the hack.
	let b:ale_enabled = 0
endfunction


" PopUp Menu:
autocmd InsertLeavePre * if pumvisible() == 0|silent!  pclose|endif


" SuperTab: More purist tab completion.
let g:SuperTabDefaultCompletionType = "context"
let g:SuperTabRetainCompletionDuration = "session"

inoremap <silent> <expr> <M-t> ToggleSuperTab()
noremap <silent> <M-t> :call ToggleSuperTab()<CR>

function! ToggleSuperTab()
	if ! exists("b:SuperTabDisabled")
		let b:SuperTabDisabled = 0
	endif

	if b:SuperTabDisabled
		let b:SuperTabDisabled = 0
		echo 'Supertab Enabled.'
	else
		let b:SuperTabDisabled = 1
		echo 'Supertab Disabled.'
	endif
	return ''
endfunc

" Use syntaxcomplete if no other omnifunc has been set.
autocmd Filetype *
			\	if &l:omnifunc == "" |
			\		setlocal omnifunc=syntaxcomplete#Complete |
			\	endif
set completefunc=syntaxcomplete#Complete


" Skeletons:
nnoremap <leader>cpp :-1read $HOME/.config/nvim/skeletons/skeleton.cpp<CR>
nnoremap <leader>shtml :-1read $HOME/.config/nvim/skeletons/skeleton.html<CR>



" ALE: Fall-back linting if there is no LSP server.
let g:ale_linters = {
			\ 'cs': ['OmniSharp','mcs'],
			\ 'java': ['checkstyle'],
			\ 'vim': ['vint'],
			\ 'cmake': ['cmakelint'],
			\ } 
let g:ale_lint_on_text_changed = 1
let b:ale_c_parse_makefile = 1
let g:ale_c_parse_makefile = 1

" AutoPairs:
" Back Tick Matching:
autocmd FileType groff,nroff,tex,latex,bc let g:AutoPairs['`']="'"

" HTML:
let g:user_emmet_leader_key=''
let g:user_emmet_mode='i'

" Noweb:
let noweb_code_languages="python,c,cpp"
augroup nowebauto
	autocmd BufRead,BufNewFile *.nw setlocal filetype=noweb
augroup END


" Vimwiki:
let g:vimwiki_list = [{
			\ 'template_default': 'def_template',
			\ 'template_ext': '.html',
			\ 'maxhi': 1
			\}]
let g:vimwiki_ext2syntax = {'.mkd': 'markdown',
			\ '.wiki': 'media'}

" PDF Viewing:
autocmd FileType groff,nroff,tex,plaintex,latex
			\ map <buffer> <F8> :!zathura<space>%:r.pdf<space>&<enter><enter>
autocmd FileType pdf,postscr map <buffer> <F8> :!zathura<space>%<space>&

" Markdown:
let g:vim_markdown_new_list_item_indent = 0

function! CountCharsWordsLines()
  let w = wordcount()
  let l = getbufinfo(bufname())[0].linecount
  echo w.chars . "\t" w.words . "\t" . l
endfunction



" Groff And Nroff:
if (expand('%:e') == 'ind' || expand('%:e' == 'qrf') || expand('%:e' == 'lit'))
	setlocal filetype=groff
endif

" LaTeX And TeX:
let g:tex_flavor = "latex"
let g:tex_fold_enabled=1
let g:tex_nospell=0
let g:vimtex_fold_enabled=1
let g:vimtex_quickfix_autoclose_after_keystrokes=1
let g:vimtex_quickfix_autoclose_after_keystrokes=1


" Commands:

" Config Access:
command! Config edit $HOME/.config/nvim/general-config.vim
command! SConfig sp | Config
command! VConfig vs | Config
command! ClearTrailingWhitespace %s/\s\+$//

command! MakeTags !ctags -R &path
command! -nargs=1 H helpgrep <args>

" Terminal Debugger:
function! TermdebugSetup(myfile)
	echo "Debugging" a:myfile
	Termdebug a:myfile
	wincmd L
	wincmd h
	wincmd j
	15wincmd _
	wincmd l
endfunc
command! -nargs=1 TermdebugNice call TermdebugSetup('<args>')

" Vim Fandom Commands:

" Decimal To Hexadecimal:
command! -nargs=? -range Dec2hex call s:Dec2hex(<line1>, <line2>, '<args>')
function! s:Dec2hex(line1, line2, arg) range
	if empty(a:arg)
		if histget(':', -1) =~# "^'<,'>" && visualmode() !=# 'V'
			let cmd = 's/\%V\<\d\+\>/\=printf("0x%x",submatch(0)+0)/g'
		else
			let cmd = 's/\<\d\+\>/\=printf("0x%x",submatch(0)+0)/g'
		endif
		try
			execute a:line1 . ',' . a:line2 . cmd
		catch
			echo 'Error: No decimal number found'
		endtry
	else
		echo printf('%x', a:arg + 0)
	endif
endfunction

" Hexadecimal To Decimal:
command! -nargs=? -range Hex2dec call s:Hex2dec(<line1>, <line2>, '<args>')
function! s:Hex2dec(line1, line2, arg) range
	if empty(a:arg)
		if histget(':', -1) =~# "^'<,'>" && visualmode() !=# 'V'
			let cmd = 's/\%V0x\x\+/\=submatch(0)+0/g'
		else
			let cmd = 's/0x\x\+/\=submatch(0)+0/g'
		endif
		try
			execute a:line1 . ',' . a:line2 . cmd
		catch
			echo 'Error: No hex number starting "0x" found'
		endtry
	else
		echo (a:arg =~? '^0x') ? a:arg + 0 : ('0x'.a:arg) + 0
	endif
endfunction

" Scroll Offset:
function! ToggleScrolloff()
	if &l:scrolloff == -1
		setlocal scrolloff=9999
	else
		setlocal scrolloff=-1
	endif
endfunc

function! GetScrolloff()
	if &l:scrolloff == -1
		return &g:scrolloff
	else
		return &l:scrolloff
	endif
endfunc


" This function returns the highlighting type of a term, making it easier to
" edit syntax highlighting for different languages.
function! <SID>SynStack()
	if !exists("*synstack")
		return
	endif
	echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

function! CloseFloats()
	for win in nvim_list_wins()
		if nvim_win_get_config(win).relative != ""
			call nvim_win_close(win, v:true)
		endif
	endfor
endfunction
