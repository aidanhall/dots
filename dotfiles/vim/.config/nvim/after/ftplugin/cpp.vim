let &l:define = '^\(#\s*define\|[a-z]*\s*const\s*[a-z]*\)'
setlocal list
" setlocal foldmethod=syntax
command! CppTags !ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extras=+q
map <buffer> <F11> :CppTags<CR>

let g:cpp_class_scope_highlight = 1
let g:cpp_posix_standard = 1
