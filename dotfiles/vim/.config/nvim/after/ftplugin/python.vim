" Python:
augroup pythonauto
	" I only want indent guides for python,
	" since the plugin handles 4-space indents
	" and this is the only language where the code will be complicated enough
	" to justify them, but will be incompatible with listchars.
	autocmd BufEnter *.py IndentGuidesEnable
	autocmd BufLeave *.py IndentGuidesDisable
	autocmd BufEnter *.py TSBufDisable highlight
augroup END

