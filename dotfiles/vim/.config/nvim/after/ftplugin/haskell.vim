" Key Bindings (Opinionated)
if has("nvim")
    nnoremap <F5> :sp term://ghci<CR>
    nnoremap <F6> :sp term://stack ghci<CR>
else
    nnoremap <F5> :term ghci<CR>
    nnoremap <F6> :term stack ghci<CR>
endif

" Compiler; not hugely useful.
compiler! ghc

" Macro Definitions
let &l:define = "^\\s*\\ze\\i\\+\\s*::"

" At least local file imports (for gf etc.).
let &l:include = "^\s*import\s*"
setlocal includeexpr=substitute(v:fname,'\\.','/','g')
setlocal suffixesadd+=.hs

" Formatting
if executable("hindent")
    setlocal formatprg=hindent
endif
