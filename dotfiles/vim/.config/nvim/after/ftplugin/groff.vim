let &l:include = '^\.\s*\(do\s\+\)*[mp]*so\s*'
let &l:define ='^\.\s*\(\(d[es]\|nr\|MAC\|al[sn]\|ALIA[SN]\)\s\+\|SETR\s\+"*\)'
setlocal path+=/usr/share/groff/current/tmac
let b:SuperTabDefaultCompletionType = "<c-x><c-d>"

" Indent
setlocal softtabstop=0 shiftwidth=0 nocindent
" Character substitution (just use preconv - groff -k - instead).
setlocal equalprg=chsub
" Comments
let &l:commentstring = ".\\\" %s"
