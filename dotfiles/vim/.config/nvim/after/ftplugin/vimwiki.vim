nnoremap <buffer> <leader>b <cmd>Vimwiki2HTMLBrowse<CR>
iabbrev <buffer> pr practice
iabbrev <buffer> Pr Practice
iabbrev <buffer> qu question
iabbrev <buffer> qus questions
iabbrev <buffer> flcs flash cards
iabbrev <buffer> Flcs Flash cards
