syn region asmString start=+\(L\|u\|u8\|U\|R\|LR\|u8R\|uR\|UR\)\="+ skip=+\\\\\|\\"+ end=+"+ 
syn region asmPointer start='\[' end='\]'
syn match asmCharacter "L\='[^\\]'"


hi def link asmCharacter Character
hi def link asmString String
hi def link asmPointer Identifier
