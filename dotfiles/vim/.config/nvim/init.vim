" Shared Vim Config:
runtime general-config.vim

" Compile Scripts:
nnoremap <F5> :!$HOME/scripts/compile1.sh %:p<CR>
nnoremap <F6> :sp<CR>:terminal $HOME/scripts/compile2.sh %:p <CR>
nnoremap <F29> :te $HOME/scripts/autocomp.sh % 1<CR>:echo "Autocomp started."<CR>
nnoremap <F30> :te $HOME/scripts/autocomp.sh % 2<CR>:echo "Autocomp started."<CR>
nnoremap <F31> :te $HOME/scripts/autocomp.sh % 3<CR>:echo "Autocomp started."<CR>

" NeoVim Terminal:
autocmd TermEnter * setlocal nonumber norelativenumber
autocmd TermLeave * setlocal number relativenumber
autocmd TermOpen * startinsert
autocmd BufEnter term://* startinsert

nnoremap <F2> :sp term://$SHELL <CR>

" GUI Colours:
set termguicolors
nnoremap <F21> :set termguicolors!<CR>
