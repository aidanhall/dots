lua << EOF
require'nvim-treesitter.configs'.setup {
	ignore_install = { "python", "latex" },
	highlight = {
		enable = true,
	},
	indent = {
		enable = false,
	},
	incremental_selection = {
		enable = true,
	},
}
EOF
