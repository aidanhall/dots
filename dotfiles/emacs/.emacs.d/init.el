;; -*- lexical-binding: t; -*-
;;; Former Custom Settings
;; This used to be my custom-set-variables list.
;; TODO: Move these settings to the relevant sections over time.
(setopt
 custom-file (locate-user-emacs-file "emacs-custom.el")
 inhibit-message-regexps '("^\\(Sound\\|Mic\\) \\[\\(on\\|off\\)\\]$" "^New volume value:")
 set-message-functions '(inhibit-message set-minibuffer-message)
 auth-source-save-behavior nil
 backup-directory-alist '((".*" . "~/.emacs.d/backups/"))
 bookmark-default-file "~/Documents/emacs/bookmarks"
 abbrev-file-name "~/Documents/emacs/abbrev"
 bookmark-save-flag 1
 calendar-week-start-day 1
 dap-java-terminal 'externalTerminal
 desktop-load-locked-desktop 'check-pid
 doc-view-continuous t
 ede-project-directories '("/home/aidan/programming/libs/ecs/tecs")
 ediff-merge-split-window-function 'split-window-vertically
 ediff-split-window-function 'split-window-horizontally
 ediff-window-setup-function 'ediff-setup-windows-plain
 elcord-idle-message "Doing something else..."
 eldoc-echo-area-prefer-doc-buffer t
 eldoc-idle-delay 0.1
 elpher-default-url-type "gemini"
 flymake-mode-line-lighter "FM"
 focus-follows-mouse t
 grep-find-ignored-directories
 '("SCCS" "RCS" "CVS" "MCVS" ".src" ".svn" ".git" ".hg" ".bzr" "_MTN" "_darcs" "{arch}" ".ccls-cache")
 help-enable-variable-value-editing t
 image-auto-resize 'fit-window
 inferior-octave-startup-args '("-i" "--line-editing")
 isearch-wrap-pause 'no
 markdown-fontify-code-blocks-natively t
 modus-themes-variable-pitch-ui t
 modus-themes-mixed-fonts t
 newsticker-auto-mark-filter-list nil
 newsticker-hide-old-items-in-newsticker-buffer t
 newsticker-retrieval-interval 0
 newsticker-url-list-defaults nil
 page-delimiter "^\\(\14\\|.*§\\)"
 pdf-view-display-size 'fit-page
 plantuml-default-exec-mode 'executable
 prettify-symbols-unprettify-at-point 'right-edge
 reb-re-syntax 'string
 reftex-cite-format 'biblatex
 reftex-include-file-commands '("include" "input" "subfile" "subfileinclude")
 reftex-ref-style-default-list '("Default" "Hyperref")
 repeat-exit-timeout 1.0
 scheme-program-name "guile"
 scroll-conservatively 101
 send-mail-function 'smtpmail-send-it
 set-mark-command-repeat-pop t
 tab-bar-show 1
 tramp-remote-path
 '(tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin" "/usr/local/bin" "/usr/local/sbin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin" "/opt/bin" "/opt/sbin" "/opt/local/bin" "/snap/bin" "/usr/local/go/bin" "~/.local/bin" "/usr/lib/ccache")
 undo-tree-history-directory-alist '((".*" . "~/.emacs.d/undo-tree/"))
 viper-ex-style-editing nil
 viper-no-multiple-ESC nil
 viper-syntax-preference 'extended
 viper-vi-style-in-minibuffer nil
 viper-want-ctl-h-help t
 vundo-window-max-height 10
 warning-suppress-types '((use-package)))
(with-eval-after-load 'calendar
  (calendar-set-date-style 'european))

(load-theme 'modus-operandi)

;;; Package Manager

(setq package-native-compile t)
;; Add melpa package archive.
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("stable-melpa" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("aelpa" . "https://aidanhall.gitlab.io/aelpa/"))
(setopt package-archive-priorities '(("aelpa" . 2) ("gnu" . 1) ("nongnu" . 2)
                                   ("stable-melpa" . 0) ("melpa" . -1)))
(setq package-archive-upload-base "~/programming/emacs/aelpa/")
(package-initialize)

;;; MISCELLANEOUS SETTINGS
(setq narrow-to-defun-include-comments t)
(setq mouse-autoselect-window 0.2)
(setq display-line-numbers-grow-only t)
(setq sentence-end-double-space nil)

;; This needs to be pre-declared, so I put it way up here.
(defun prettify-these (&rest pairs)
  "Add the given symbol-conversion PAIRS top `prettify-symbols-alist'."
  (dolist (pair pairs)
    (setf (alist-get (car pair) prettify-symbols-alist) (cadr pair))))

;; This function is from Xah Lee: http://xahlee.info/emacs/emacs/elisp_read_file_content.html
(defun get-string-from-file (file-path)
  "Return FILE-PATH's file content."
  (with-temp-buffer
    (insert-file-contents file-path)
    (buffer-string)))

(add-hook 'prog-mode-hook 'goto-address-prog-mode)
(add-hook 'text-mode-hook 'goto-address-mode)

;;; BINDINGS

;;;; Miscellaneous.
(bind-key "C-<menu>" 'execute-extended-command)
(bind-key "<269025062>" 'previous-buffer)
(bind-key "<269025063>" 'next-buffer)
(bind-key "H-[" 'previous-buffer)
(bind-key "H-]" 'next-buffer)
(bind-key "M-[" 'previous-error)
(bind-key "M-]" 'next-error)
(bind-key "C-c u" 'browse-url-at-point)
(bind-key "k" 'kill-current-buffer ctl-x-map)
(bind-key "M-k" 'kill-buffer ctl-x-map)
(bind-key "C-c j" 'duplicate-dwim)
(bind-key "C-." 'duplicate-dwim)
(bind-key "C-," 'copy-from-above-command)
(bind-key "H-h" 'help-command)
(bind-key "H-r" 'xref-find-references)
(bind-key "H-." 'xref-find-definitions)
(bind-key "H-u" 'browse-url)
(bind-key "C-M-z" 'zap-up-to-char)
(bind-key "M-c" 'capitalize-dwim)
(bind-key "M-u" 'upcase-dwim)
(bind-key "M-l" 'downcase-dwim)

(bind-key "H-," 'switch-to-buffer-other-window)
(bind-key "H-SPC" 'switch-to-buffer)
(bind-key "H-b" 'consult-buffer)
;; Totally original, arbitrary register keybindings
(bind-key "H-m" 'consult-register-store)
(bind-key "H-'" 'consult-register-load)

(bind-key "<Scroll_Lock>" 'ignore)

(bind-key "l" 'tab-list tab-prefix-map)

(defun mark-around-sexp ()
  "Set mark around the sexp point is in."
  (interactive)
  (backward-up-list)
  (mark-sexp))
(defun mark-in-sexp ()
  "Set mark in the sexp point is in."
  (interactive)
  (mark-around-sexp)
  (forward-char)
  (set-mark (1- (mark))))
(bind-key "C-M-g" 'mark-in-sexp)
;;;; Window Management Double Ups
(bind-key "C-1" 'delete-other-windows ctl-x-map)
(bind-key "C-2" 'split-window-below ctl-x-map)
(bind-key "C-3" 'split-window-right ctl-x-map)
(bind-key "C-4" 'ctl-x-4-prefix ctl-x-map)
(bind-key "C-5" 'ctl-x-5-prefix ctl-x-map)

;;;; Describe Map
(use-package help
  :custom
  (help-window-select t)
  :bind
  (:map help-map
        ("8" . describe-char)
        ("=" . describe-char)
        ("j" . describe-face)))

;;;; Menu Key Portability Hack
(define-key key-translation-map (kbd "<apps>") (kbd "<menu>"))
(define-key key-translation-map (kbd "<mouse-8>") (kbd "<XF86Back>"))
(define-key key-translation-map (kbd "<mouse-9>") (kbd "<XF86Forward>"))

;;;; Building
(defun universal-script-compile (&optional arg)
  "Run `compile' with one of my compilation scripts.
The compile script with (numeric prefix argument) number ARG.
ARG defaults to 1, so the default compilation script is compile1.sh"
  ;; Defaulting ARG to 1, as in `next-line'.
  (interactive "p")
  (if (null arg)
      (setq arg 1))

  ;; Construct the compile string.
  (setq-local
   compile-command
   (concat
    "~/scripts/compile"
    (number-to-string arg)
    ".sh "
    (buffer-file-name)
    " "))
  ;; Prompt the user, with the `compile-command' already inserted to check it.
  ;; This is so it behaves consistently with calling `compile' or
  ;; `project-compile' normally.
  (call-interactively #'compile))
(bind-key "C-<f7>" 'compile)
(bind-key "<f7>" 'recompile)
(bind-key "S-<f7>" 'universal-script-compile)
(bind-key "M-<f7>" 'project-compile)

;;; Direnv

(use-package direnv
  :ensure t)

;;; Buffers

(defalias 'list-buffers 'ibuffer)
(use-package ibuffer
  :ensure t
  :defer t
  :custom
  (ibuffer-use-other-window t)
  :bind
  (:map ctl-x-map
        ("C-b" . ibuffer)
        ("M-b" . ibuffer-bs-show)))

;;; Essential (?) Packages
;;;; Mode Local Functions
(require 'mode-local)

;;;; Devil Mode
(use-package devil
  :ensure t
  :bind (:map global-map
              ("C-z" . global-devil-mode))
  :delight devil-mode
  :config
  (global-devil-mode 1))

;;;; evil
(use-package evil
  :ensure t
  :defer t
  :init
  (setq evil-want-keybinding nil)
  :custom
  (evil-want-keybinding nil)
  (evil-vsplit-window-right t)
  (evil-split-window-below t)
  (evil-respect-visual-line-mode t)
  (evil-undo-system 'undo-redo)
  (evil-want-Y-yank-to-eol t)
  :config
  (evil-set-leader '(normal motion) (kbd "SPC")))
(use-package evil-collection
  :ensure t
  :after evil
  :delight evil-collection-unimpaired-mode)

;;;; Dired
(require 'dired-x)
(require 'dired-aux)
(require 'wdired)
(nconc dired-omit-extensions
       '(".bcf" ".fdb_latexmk" ".fls"))
(setopt dired-create-destination-dirs 'ask
        dired-listing-switches "-alh"
        dired-guess-shell-alist-user '((".*" "xdg-open"))
        dired-dwim-target 'dired-dwim-target-next
        wdired-allow-to-change-permissions t)
(with-eval-after-load 'evil
  (evil-define-key 'motion dired-mode-map
    (kbd "-") 'dired-up-directory
    (kbd "M-o") 'dired-display-file
    (kbd "RET") 'dired-find-file
    (kbd "TAB") 'dired-hide-subdir
    (kbd "+") 'dired-create-directory
    (kbd "<mouse-1>") 'dired-insert-subdir)
  (evil-define-key 'normal dired-mode-map
    (kbd "SPC") 'evil-send-leader))

;;;; Speedbar
(use-package speedbar
  :defer t
  :config
  (cl-pushnew
   '(name . "Speedbar")
   speedbar-frame-parameters
   :key 'car)
  (with-eval-after-load 'evil
    (evil-set-initial-state 'speedbar-mode 'emacs)))

;;;; Search
(with-eval-after-load 'evil
  (evil-define-key 'normal occur-mode-map
    (kbd "o") 'occur-mode-display-occurrence))
(use-package rg
  :ensure t
  :defer t
  :bind  ("<f2>" . rg))
(bind-keys
 ("M-s i" . consult-line)
 ("C-<f2>" . consult-ripgrep))
(setq search-whitespace-regexp "[ \t\r\n]+")

;;;; Docker
(use-package dockerfile-mode
  :ensure t
  :defer t)
(use-package docker
  :ensure t
  :defer t)
;;; Programming

(add-hook 'prog-mode-hook
          (lambda()
            (prettify-these
             '("<=" "≤")
             '(">=" "≥")
             '("!=" "≠")
             '("==" "≡"))
            (setq
             truncate-lines t
             fill-column 100
             whitespace-line-column nil)))

;; Help
(bind-key "<tab>" 'forward-button help-mode-map)

;;; Terminals/Shells
(bind-keys
 ("<f5>" . eshell)
 ("C-<f5>" . eat)
 ("S-<f5>" . shell)
 ("<f6>" . project-eshell)
 ("C-<f6>" . eat-project)
 ("S-<f6>" . project-shell)
 ("M-*" . project-async-shell-command))

;; Directly emit long outputs into the shell buffer.
;; This prevents programs like guix and git from calling less,
;; which doesn't work properly in most Emacs shell interfaces.
;; The exceptions are terminal emulators.
(setenv "PAGER" "cat")

(defun shell-config ()
  "The configuration for Emacs shells."
  (goto-address-mode 1)
  (add-hook 'evil-insert-state-entry-hook
            (lambda () (setq display-line-numbers nil))
            0
            1))

(use-package eshell
  :defer 10
  :custom
  (eshell-history-size nil)
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal eshell-mode-map
      (kbd "^") 'eshell-bol))
  (with-eval-after-load 'em-hist
    (bind-key "M-r" 'eshell-isearch-backward eshell-hist-mode-map)
    (bind-key "M-s" 'eshell-isearch-forward eshell-hist-mode-map)))
(use-package vterm
  :custom
  (vterm-max-scrollback 10000)
  ;; less works in vterm, and works around its scrollback limit
  (vterm-environment '("PAGER=less"))
  :config
  ;; The customization type is currently incorrect, so we can't use custom or setopt.
  (setq vterm-tramp-shells '(("ssh" "/usr/bin/bash") ("docker" "/bin/sh"))))
(use-package eat
  :defer t
  :ensure t)

(add-hook 'eshell-mode-hook 'shell-config)
(add-hook 'shell-mode-hook 'shell-config)
(add-hook 'term-mode-hook 'shell-config)

(with-eval-after-load 'tramp
  (setf (cadr (assoc 'tramp-remote-shell (assoc "ssh" tramp-methods)))
        "/bin/bash"))
;;; IDE Features
;;;; Bug References
(with-eval-after-load 'bug-reference
  (setq bug-reference-setup-from-vc-alist
        (list
         (list
          (rx (one-or-more alpha) "://" (group (one-or-more print)) ".arm.com")
          (rx (group
               (or (seq "change-" (group (one-or-more numeric)))
                   (group (one-or-more alpha) "-" (one-or-more digit))
                   (seq "Change-Id: " (group (one-or-more alnum)))
                   (seq "TI2: " (group (one-or-more numeric))))))
          (lambda (url-groups)
            (let* ((gerrit-domain (concat "https://"
                                          (let ((gerrit-host (cadr url-groups)))
                                            (if (string-search "gerrit" gerrit-host)
                                                gerrit-host
                                              "eu-gerrit-1.euhpc"))
                                          ".arm.com/"))
                   (urls (list (concat gerrit-domain "c/")
                               "https://jira.arm.com/browse/"
                               (concat gerrit-domain "q/")
                               "https://ti2.gpu.arm.com/tables/run/")))
              (lambda ()
                (cl-loop for i from 2
                         for url in urls
                         for match = (match-string i)
                         until match
                         finally return (concat url match)))))))))
(add-hook 'magit-mode-hook 'bug-reference-mode)
(add-hook 'log-view-mode-hook 'bug-reference-mode)
;;;; Eglot
(use-package eglot
  :ensure t
  :custom-face
  (eglot-highlight-symbol-face ((t (:inherit lazy-highlight))))
  :bind
  (:map eglot-mode-map
        ("H-d" . eglot-find-declaration)
        ("H-t" . eglot-find-typeDefinition)
        ("H-i" . eglot-find-implementation)
        ("H-a" . eglot-code-actions)
        ("H-n" . eglot-rename)
        ("H-l" . eglot-format)
        ("C-c c" . eglot-reconnect)
        ("C-c d" . eglot-find-declaration)
        ("C-c t" . eglot-find-typeDefinition)
        ("C-c i" . eglot-find-implementation)
        ("C-c a" . eglot-code-actions)
        ("C-c r" . eglot-rename)
        ("C-c f" . eglot-format))
  :config
  (easy-menu-remove-item nil nil "Eglot")
  (easy-menu-add-item nil nil eglot-menu)
  (with-eval-after-load 'evil
    (evil-define-key 'normal eglot-mode-map
                     "ga" 'eglot-code-actions
                     "gR" 'eglot-rename
                     (kbd "<leader>l") 'eglot-format))
  (push `((LaTeX-mode TeX-mode AmSTeX-mode ConTeXt-mode docTeX-mode
                      plain-TeX-mode japanese-plain-TeX-mode japanese-LaTeX-mode)
          . ,(eglot-alternatives '("digestif" "texlab")))
        eglot-server-programs)
  (push `((c-mode c-ts-mode c++-mode c++-ts-mode)
          . ,(eglot-alternatives '(("clangd" "-header-insertion=never") "ccls")))
        eglot-server-programs)

  (add-hook 'eglot-managed-mode-hook
            (lambda()
              (setq-local eldoc-documentation-strategy 'eldoc-documentation-compose-eagerly))
            100))

(use-package eglot-java
  :ensure t
  :defer t)

;;;; Devdocs
(use-package devdocs
  :ensure t
  :defer t
  :custom
  (devdocs-window-select help-window-select)
  :init
  (defmacro my-devdocs-default-docs (&rest entries)
    "Set `devdocs-current-docs' appropriately for each of the ENTRIES.

Each entry should be of the form (MODES . DOCUMENTS), where MODE
is a (list of) major-mode symbol(s) and DOCUMENTS is the inital value to set
for `devdocs-current-docs' in that mode."
    `(progn
       ,@ (mapcar
           (lambda (entry)
             (let ((modes (car entry))
                   (docs (cdr entry)))
               `(progn
                  ,@(mapcar
                     (lambda (mode)
                       `(setq-mode-local ,mode
                                         devdocs-current-docs ',(cdr entry)))
                     (if (listp modes) modes (list modes))))))
           entries)))
  (my-devdocs-default-docs
   ((c-mode c-ts-mode) "c")
   ((c++-mode c++-ts-mode) "cpp")
   (glsl-mode "opengl~4")
   ((html-mode css-mode css-ts-mode js-mode js-ts-mode)
    "html" "css" "javascript"))
  :bind
  (:map help-map
        (";" . devdocs-lookup)))
;;;; In-Buffer Completion
(setq tab-always-indent 'complete)

(use-package corfu
  :ensure t
  :custom
  (corfu-cycle t)
  (corfu-auto t)
  (corfu-auto-delay 0.05)               ; 0.0 makes kmacros lag
  (corfu-auto-prefix 2)
  (corfu-popupinfo-delay '(0.5 . 0.25))
  (corfu-on-exact-match nil)
  :hook
  ((emacs-lisp-mode)
   . corfu-mode))
(add-hook 'corfu-mode-hook 'corfu-popupinfo-mode)
(add-hook 'corfu-mode-hook
          (lambda ()
            (setq-local completion-styles '(basic))))

(defun basic-ide()
  "Set up a basic IDE, using LSP."
  (unless (file-remote-p default-directory)
    (eglot-ensure))
  (corfu-mode 0)
  (company-mode 1))

;;; Delight
(use-package delight
  :ensure t)

(delight 'auto-revert-mode " 🌀" 'autorevert)
(delight 'eldoc-mode " 📚" 'eldoc)
(delight 'glasses-mode " 👓" 'glasses)

;;; languages/file types
;;;; Tree Sitter
(setq treesit-language-source-alist
      '((bash "git@github.com:tree-sitter/tree-sitter-bash")
        (cmake "git@github.com:uyha/tree-sitter-cmake")
        (css "git@github.com:tree-sitter/tree-sitter-css")
        (elisp "git@github.com:Wilfred/tree-sitter-elisp")
        (go "git@github.com:tree-sitter/tree-sitter-go")
        (gomod "git@github.com:camdencheek/tree-sitter-go-mod")
        (html "git@github.com:tree-sitter/tree-sitter-html")
        (javascript "git@github.com:tree-sitter/tree-sitter-javascript" "master" "src")
        (json "git@github.com:tree-sitter/tree-sitter-json")
        (make "git@github.com:alemuller/tree-sitter-make")
        (markdown "git@github.com:ikatyang/tree-sitter-markdown")
        (python "git@github.com:tree-sitter/tree-sitter-python")
        (rust "git@github.com:tree-sitter/tree-sitter-rust")
        (toml "git@github.com:tree-sitter/tree-sitter-toml")
        (tsx "git@github.com:tree-sitter/tree-sitter-typescript" "master" "tsx/src")
        (typescript "git@github.com:tree-sitter/tree-sitter-typescript" "master" "typescript/src")
        (cpp "git@github.com:tree-sitter/tree-sitter-cpp")
        (c "git@github.com:tree-sitter/tree-sitter-c")
        (yaml "git@github.com:ikatyang/tree-sitter-yaml")))
;; Install these with the following command:
(defun treesit-install-listed-grammars ()
  "All tree-sitter grammars in `treesit-language-source-alist' are installed."
  (interactive)
  (dolist (lang treesit-language-source-alist)
    (unless (treesit-language-available-p (car lang))
       (treesit-install-language-grammar (car lang)))))
(setopt major-mode-remap-alist
        (nconc '((js-mode . js-ts-mode)
		 (css-mode . css-ts-mode))
               major-mode-remap-alist))
;;;; Task Juggler
(use-package tj3-mode
  :ensure t
  :mode "\\.tjp\\'"
  :defer t)
;;;; C
(defun personal-c-config ()
  (interactive)
  (basic-ide))
(use-package c-ts-mode
  :ensure t
  :defer t
  :config
  (add-hook 'c-ts-mode-hook 'personal-c-config)
  (add-hook 'c++-ts-mode-hook 'personal-c-config)
  (c-ts-mode-set-global-style 'linux))
(add-hook 'c-mode-hook 'personal-c-config)
(add-hook 'c++-mode-hook 'personal-c-config)

;;;; Lisp
(add-hook 'lisp-data-mode-hook 'flymake-mode)
(add-hook 'scheme-mode 'flymake-mode)
(setq inferior-lisp-program "sbcl")

(use-package paredit
  :ensure t
  :delight paredit-mode
  :hook
  ((inferior-scheme-mode
    scheme-mode
    inferior-lisp-mode
    lisp-data-mode
    sly-mrepl-mode
    geiser-repl-mode)
   . paredit-mode)
  :config
  (keymap-unset paredit-mode-map "RET"))

(use-package rainbow-delimiters
  :ensure t
  :hook ((lisp-data-mode
          scheme-mode
          inferior-lisp-mode
          sly-mrepl-mode
          emacs-lisp-mode)
         . rainbow-delimiters-mode))

;; Scheme and Geiser
(use-package geiser
  :ensure t
  :defer t
  :config
  (add-hook 'geiser-mode-hook 'corfu-mode)
  (add-hook 'geiser-repl-mode-hook 'corfu-mode)
  (with-eval-after-load 'evil
    (add-hook 'geiser-mode-hook 'evil-collection-geiser-setup)))
(use-package geiser-guile
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal geiser-mode-map
      (kbd "<leader>SPC") 'geiser-eval-last-sexp
      (kbd "<f8>") 'geiser)))

;; Sly
(use-package sly
  :ensure t
  :defer t
  :config
  (add-hook 'sly-mode-hook 'corfu-mode)
  (defun sly-mrepl-other-window ()
    "Switch to the sly mrepl in an `other-window'."
    (interactive)
    (sly-mrepl #'switch-to-buffer-other-window))
  (with-eval-after-load 'evil
    (evil-define-key 'normal sly-editing-mode-map
      (kbd "<leader>SPC") 'sly-eval-defun
      (kbd "<f7>") 'sly-compile-and-load-file
      (kbd "<f8>") 'sly)))


;;;; Conf
(add-hook 'conf-mode-hook 'outline-minor-mode)
;;;; CSV
(use-package csv-mode
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal csv-mode-map
      (kbd "TAB") 'csv-tab-command
      (kbd "S-TAB") 'csv-backtab-command)))

(use-package glsl-mode :ensure t :defer t)

;;;; Go
(use-package go-ts-mode
  :ensure t
  :defer t
  :config
  (add-hook 'go-ts-mode-hook 'basic-ide))
(use-package zig-mode
  :ensure t
  :defer t
  :custom
  (zig-format-on-save nil)
  :config
  (add-hook 'zig-mode-hook 'basic-ide))
;;;; Rust
(use-package rust-ts-mode
  :ensure t
  :defer t
  :hook (rust-ts-mode . basic-ide))
(add-to-list 'auto-mode-alist '("\\Cargo.lock\\'" . conf-toml-mode))

;;;; Emacs Lisp
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (hs-minor-mode 0)
            (outline-minor-mode 1))
          50)
(with-eval-after-load 'evil
  (evil-define-key 'normal emacs-lisp-mode-map
    (kbd "<leader>SPC") 'eval-last-sexp))

;;;; Erlang
(use-package erlang
  :ensure t
  :defer t
  :config
  (setq inferior-erlang-shell-type 'oldshell))

;;;; Gnuplot
(use-package gnuplot
  :ensure t
  :defer t
  :init
  (push '("\\.gnu\\'" . gnuplot-mode) auto-mode-alist))

;;;; Haskell
(use-package haskell-mode
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal haskell-mode-map
      (kbd "<leader>SPC") 'haskell-process-load-file
      (kbd "<leader>c") 'haskell-interactive-switch))
  (add-hook 'haskell-mode-hook 'basic-ide)
  (add-hook 'haskell-interactive-mode-hook
            (lambda ()
              (corfu-mode 1)
              (evil-define-key 'normal 'local
                (kbd "<leader>c") 'haskell-interactive-switch-back
                (kbd "<leader>SPC") 'haskell-interactive-switch-back
                (kbd "^") 'haskell-interactive-mode-bol))))

;;;; Kconfig
(use-package kconfig-mode
  :defer t
  :ensure t
  :mode (("Mconfig\\'" . kconfig-mode)))

;;;; NASM
(use-package nasm-mode
  :ensure t
  :defer t)
;;;; PlantUML
(use-package plantuml-mode
  :ensure t
  :defer t)

;;;; Godot
(use-package gdscript-mode
  :ensure t
  :defer t
  :config
  (electric-pair-local-mode 0))
(add-to-list 'auto-mode-alist '("\\project.godot\\'" . conf-mode))
(use-package crontab-mode :ensure t :defer t)

;;;; SQL
(use-package sql-indent
  :ensure t
  :after sql
  (add-hook 'sql-mode-hook 'sqlind-minor-mode))
(use-package sql
  :defer t
  :config
  (sql-set-product 'postgres))
;;;; Prolog
(use-package prolog
  :defer t
  :config
  (setq prolog-system 'gnu)
  (evil-define-key 'normal prolog-mode-map
    (kbd "<leader>SPC") 'prolog-consult-file))
;;; Version Control
(use-package magit
  :ensure t
  :defer t
  :bind
  ("C-<f10>" . magit-status))
(use-package vc
  :ensure t
  :bind
  ("<f10>" . project-vc-dir)
  :config
  (defun my-vc-dir-mark-removed-files ()
    "Mark files that are in removed state."
    (interactive)
    (vc-dir-mark-state-files 'removed))
  (defun my-vc-dir-mark-edited-files ()
    "Mark files that are in edited state."
    (interactive)
    (vc-dir-mark-state-files 'edited))
  (defun my-vc-dir-mark-added-files ()
    "Mark files that are in added state."
    (interactive)
    (vc-dir-mark-state-files 'added))
  (with-eval-after-load 'vc
    (bind-keys
     :map vc-dir-mode-map
     ("*u" . vc-dir-mark-unregistered-files)
     ("*d" . my-vc-dir-mark-removed-files)
     ("*e" . my-vc-dir-mark-edited-files)
     ("*a" . my-vc-dir-mark-added-files))))

(add-hook 'log-edit-mode-hook 'auto-fill-mode)
(add-to-list 'auto-mode-alist '("\\.gitmodules\\'" . conf-mode))
(use-package diff-hl
  :ensure t
  :hook
  (dired-mode . diff-hl-dired-mode-unless-remote)
  :config
  (global-diff-hl-mode 1))

(with-eval-after-load 'log-view
  (keymap-set log-view-mode-map "TAB" 'log-view-toggle-entry-display))


;;; Htop

(add-to-list 'auto-mode-alist '("\\htoprc\\'" . conf-mode))

;;; Windows
(use-package window
  :custom
  (max-mini-window-height 0.2)
  (display-buffer-alist
   '(("\\*Occur\\*"
      (display-buffer-reuse-window)
      (window-height . shrink-window-if-larger-than-buffer))
     ("\\*\\(Fly.*\\|eldoc.*\\)\\*"
      (display-buffer-in-side-window)
      (side . bottom))
     ("\\*.*-.*\\(shell\\|term\\|eat\\)\\*"
      (display-buffer-in-side-window)
      (side . bottom))
     ("\\*.*\\(shell\\|term\\|eat\\)\\*.*"
      (display-buffer-reuse-window))
     ("\\*\\(rustic-compilation\\|cargo-.*\\)\\*"
      (display-buffer-reuse-window)
      (inhibit-same-window t))))
  :bind
  ("<f9>" . window-toggle-side-windows)
  ("<f1>" . other-window))

;;;; ace window
(use-package ace-window
  :ensure t
  :defer t
  :bind
  ("S-<f1>" . ace-window))

;;; consult
(use-package consult
  :ensure t
  :init
  (setq consult-project-root-function (lambda () (project-root (project-current))))
  (define-minor-mode consult-xref-mode
    "Toggle using consult to display xref results.
See `consult-xref' and `xref-show-xrefs-function'."
    :lighter " CX"
    (if consult-xref-mode
        (setq-local xref-show-xrefs-function 'consult-xref)
      (setq-local xref-show-xrefs-function (default-value 'xref-show-xrefs-function)))))
(bind-keys
 :prefix-map personal-consult-map
 :prefix "s-\\"
 ("b" . consult-buffer)
 ("i" . consult-imenu)
 ("I" . consult-imenu-multi)
 ("l" . consult-line)
 ("L" . consult-line-multi)
 ("g" . consult-ripgrep)
 ("o" . consult-outline))


;;; General Evil Keybindings and Configuration
(with-eval-after-load 'evil
  (evil-define-key '(normal motion) 'global
    (kbd "C-w f") 'ffap-other-window
    (kbd "gO") 'consult-imenu-multi
    (kbd "<leader>i") 'consult-imenu
    (kbd "<leader>I") 'consult-imenu-multi
    (kbd "<leader>u") 'vundo
    (kbd "<leader>g") 'magit-status
    (kbd "<leader>G") 'magit-file-dispatch
    (kbd "<leader>v") 'vc-prefix-map
    (kbd "<leader>m") 'switch-to-buffer
    (kbd "<leader>,") 'switch-to-buffer-other-window
    (kbd "<leader>M") 'consult-buffer
    (kbd "<leader><") 'consult-buffer-other-window
    (kbd "<leader>f") 'find-file
    (kbd "<leader>r") 'find-file-other-window
    (kbd "<leader>d") 'find-file
    (kbd "-") 'dired-jump
    (kbd "<leader>a") 'org-agenda
    (kbd "<leader>s") 'speedbar
    (kbd "<leader>/") 'ibuffer
    (kbd "<leader>w") 'other-window
    (kbd "<leader>1") 'delete-other-windows
    (kbd "<leader>2") 'split-window-vertically
    (kbd "<leader>3") 'split-window-horizontally
    (kbd "<leader>4") 'ctl-x-4-prefix
    (kbd "<leader>5") 'ctl-x-5-prefix
    (kbd "<leader>t") tab-prefix-map ;; There isn't an alias for this variable!
    (kbd "<leader>0") 'delete-window
    (kbd "<leader>q") 'delete-window
    (kbd "<leader>h") 'help-command
    (kbd "<leader>x") 'kill-buffer
    (kbd "<leader>;") 'execute-extended-command
    (kbd "zp") 'narrow-to-page
    (kbd "zd") 'narrow-to-defun
    (kbd "zn") 'narrow-to-region
    (kbd "zw") 'widen
    (kbd "æ") 'revert-buffer-quick
    (kbd "»") 'org-next-link
    (kbd "«") 'org-previous-link
    (kbd "„") 'org-mark-ring-goto))

(bind-key "M-/" 'hippie-expand)
(bind-key "M-#" 'comint-dynamic-complete-filename)

(with-eval-after-load 'evil
  (evil-set-initial-state 'newsticker-mode 'emacs)
  (evil-set-initial-state 'newsticker-treeview-mode 'emacs)
  (evil-set-initial-state 'custom-theme-choose-mode 'emacs)
  (evil-set-initial-state 'mpc-mode 'emacs)
  (evil-set-initial-state 'Man-mode 'motion)
  (evil-set-initial-state 'package-menu-mode 'motion)
  (add-hook 'view-mode-hook (lambda()
                              (if evil-mode
                                  (evil-motion-state)))))

;;;; Vim Editing Plugin Ports
(use-package evil-matchit
  :ensure t
  :after evil
  :config
  (global-evil-matchit-mode 1))
(use-package evil-commentary
  :ensure t
  :delight evil-commentary-mode
  :after evil
  :config
  (evil-commentary-mode 1))

(use-package evil-surround
  :ensure t
  :after evil
  :config
  (global-evil-surround-mode 1))

;;; Projects
(use-package project
  :bind
  (:map project-prefix-map
        ("o" . project-display-buffer)
        ("t" . project-speedbar)
        ("0" . project-gdb)
        ("g" . rg-project))
  :config
  (with-eval-after-load 'evil
    (evil-define-key '(normal motion) 'global
      (kbd "<leader>p") project-prefix-map))

  (defun project-speedbar ()
    "Run Speedbar in the current project's root"
    (interactive)
    (let ((default-directory (project-root (project-current t))))
      (speedbar-frame-mode 1)))

  (defun project-gdb ()
    "Run `gdb' in the current project's root"
    (interactive)
    (let* ((default-directory (project-root (project-current t))))
      (call-interactively #'gdb)))

  (setq project-switch-use-entire-map t)

  (or (assoc 'project-speedbar project-switch-commands)
      (nconc project-switch-commands
             '((rg-project "Run ripgrep")
               (project-speedbar "Speedbar")))))

(use-package projectile
  :defer t
  :delight projectile-mode nil
  :config
  (evil-define-key '(normal motion) projectile-mode-map
    (kbd "C-c p") 'projectile-command-map
    (kbd "<leader>v") 'projectile-command-map
    (kbd "M-*") 'projectile-run-async-shell-command-in-root
    (kbd "M-<f7>") 'projectile-compile-project
    (kbd "<f6>") 'projectile-run-eshell))

;;; Which Key
(use-package which-key
  :ensure t
  :config
  (which-key-mode 1)
  :delight which-key-mode "")

;;; Tcl

(use-package tcl
  :ensure t
  :defer t
  :bind
  (:map tcl-mode-map
        ("C-c C-r" . tcl-restart-with-file))
  :config
  (evil-define-key 'normal tcl-mode-map
    (kbd "<leader>SPC") 'tcl-eval-defun
    (kbd "<leader>j") 'tcl-restart-with-file))

;;; Emmet for HTML and CSS
(use-package emmet-mode
  :ensure t
  :hook
  sgml-mode
  html-mode
  css-mode
  markdown-mode
  nxml-mode
  :delight emmet-mode " EM"
  :pin melpa
  :custom
  (emmet-move-cursor-between-quotes t))

;;; CSS
(setq-mode-local css-mode open-paren-in-column-0-is-defun-start nil)

;;; Completion
(setopt completions-detailed t)
(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles basic partial-completion))
                                   (eglot (styles orderless basic)))))

(use-package company
  :ensure t
  :config
  (company-tng-configure-default)
  :delight company-mode
  :defer t
  :custom
  (company-idle-delay 0.0))
(use-package company-glsl
  :ensure t
  :after company)

(use-package icomplete
  :defer t
  :custom
  (icomplete-scroll 1)
  (icomplete-show-matches-on-no-input t)
  (icomplete-matches-format "%s/%s\t")
  (icomplete-max-delay-chars 5))

(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  (vertico-mouse-mode))

;;; Org
(use-package org
  :ensure
  :defer 10
  :bind
  (:map org-mode-map
        ("H-e" . org-macro-insert)
        ("H-l" . org-latex-preview))
  :bind
  ("C-c l" . org-store-link)
  :config
  (require 'org-tempo)
  (define-skeleton org-macro-insert
    "Insert a call to an Org macro."
    ""
    "{{{"
    (completing-read "Macro: " (mapcar #'car (append org-macro-templates org-export-global-macros)) nil t)
    _
    "}}}")
  (setq-mode-local org-mode display-line-numbers 'visual)
  (setopt org-export-async-init-file (locate-user-emacs-file "org-export-conf.el"))
  (load org-export-async-init-file)
  (setopt org-agenda-files (file-name-concat org-directory "agenda")
          org-default-notes-file (file-name-concat org-directory "notes.org"))
  (with-eval-after-load 'evil
    (evil-define-key 'normal org-mode-map
      (kbd "<leader>SPC") 'org-ctrl-c-ctrl-c
      (kbd "<leader>n") 'org-next-link
      (kbd "<leader>N") 'org-previous-link
      (kbd "¢") 'org-open-at-point
      (kbd "<leader>o") 'org-open-at-point
      (kbd "<leader>l") 'org-todo
      (kbd "<leader>e") 'org-export-dispatch
      (kbd "<leader>S") 'org-set-property
      (kbd "<leader>k") 'org-clock-in
      (kbd "<leader>j") 'org-clock-out
      (kbd "<leader>i") 'consult-org-heading
      (kbd "<leader>I") 'org-toggle-inline-images
      (kbd "<leader>U") 'org-latex-preview
      (kbd "<leader>b") 'org-store-link
      (kbd "<leader>c") 'org-insert-link
      (kbd "<leader>j") 'org-reftex-citation
      (kbd "<f8>") 'org-mark-ring-push
      (kbd "<f7>") 'org-mark-ring-goto
      (kbd "TAB") 'org-cycle
      (kbd "RET") 'org-open-at-point
      (kbd "DEL") 'org-mark-ring-goto
      (kbd "zs") 'org-narrow-to-subtree))

  (defun personal-org-config ()
    "My personal Org Mode configuration."
    (prettify-these
     '("[ ]" "☐")
     '("[X]" "☑")
     '("[-]" "☒")))
  (add-hook 'org-mode-hook 'personal-org-config)
  (delight 'org-indent-mode ">" 'org-indent)
  (delight 'org-num-mode "#" 'org-num)
  :custom
  (org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-id)
  (org-export-in-background t)
  (org-adapt-indentation nil)
  (org-babel-J-command "~/sources/j902/jconsole.sh")
  (org-enforce-todo-dependencies t)
  (org-export-backends '(latex))
  (org-icalendar-combined-agenda-file "~/Documents/org/org.ics")
  (org-icalendar-include-todo t)
  (org-icalendar-use-scheduled '(event-if-not-todo todo-start))
  (org-log-into-drawer t)
  (org-mobile-directory "~/Documents/org")
  (org-modules '(ol-bibtex ol-doi))
  (org-num-max-level 3)
  (org-preview-latex-default-process 'dvisvgm)
  (org-special-ctrl-a/e 'reversed)
  (org-html-format-drawer-function
   (apply-partially #'format "<details>\n<summary>%s</summary>\n%s"))
  (org-use-speed-commands t)
  (org-src-preserve-indentation t))
(use-package org-drill
  :ensure t
  :after org)
(use-package org-real
  :ensure t
  :after org)
(use-package org-pomodoro
  :ensure t
  :after org
  :bind
  (:map org-mode-map
        ("H-p" . org-pomodoro)))

(use-package denote
  :ensure t
  :after org
  :custom
  (denote-directory (file-name-concat org-directory "denote/")))
(use-package engrave-faces
  :ensure t
  :after org
  :config
  (setq org-latex-src-block-backend 'engraved))

(bind-key "<f12>" 'org-agenda)

;;; Images
(setq image-use-external-converter t)
;;; Music
(use-package mpdel
  :ensure t
  :defer 10
  :custom
  (mpdel-prefix-key (kbd "H-z")))

(use-package mpdired
  :ensure t)

;;; Markdown
(use-package markdown-mode
  :ensure t
  :defer t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode))
  :bind
  (:map markdown-mode-map
        ("C-c C-a b" . markdown-footnote-return)
        ("C-c C-a g" . markdown-footnote-goto-text))
  :config
  (add-hook 'markdown-mode-hook 'outline-minor-mode)
  :custom
  (markdown-fontify-code-block-natively t))

;;; Python
(add-hook 'python-mode-hook
          (lambda ()
            (basic-ide)
            (hs-minor-mode 0)
            (outline-minor-mode 1)))
(use-package python
  :defer t
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal python-mode-map
      (kbd "<leader>T") 'python-test-file
      (kbd "<leader>SPC") 'python-shell-send-buffer)))

;;; JavaScript
(use-package js
  :ensure t
  :defer t
  :init
  (push '("\\.mjs" . js-mode) auto-mode-alist))
(use-package prettier-js
  :ensure t
  :defer t)
(use-package typescript-mode
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal typescript-mode-map
      (kbd "<leader>SPC") 'ff-find-other-file))
  (setq  typescript-indent-level js-indent-level))
(use-package rjsx-mode
  :ensure t
  :defer t
  :init
  (push '("\\.tsx\\'" . rjsx-mode) auto-mode-alist)
  :config
  (setq
   js2-mode-show-parse-errors nil
   js2-mode-show-strict-warnings nil))
(use-package json-ts-mode
  :ensure t
  :defer t)
(cl-pushnew '("{" . json-ts-mode) magic-mode-alist)
(use-package yaml-ts-mode
  :ensure t
  :defer t)
(use-package svelte-mode
  :ensure t
  :defer t
  :config
  (with-eval-after-load 'eglot
    (push '(svelte-mode "svelteserver" "--stdio") eglot-server-programs))
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal svelte-mode-map
      (kbd "<leader>SPC") 'ff-find-other-file))
  (add-hook 'svelte-mode-hook
            (lambda ()
              ;; TODO: Make this buffer-local?
              (advice-remove 'eglot--snippet-expansion-fn #'ignore))))

(use-package js
  :defer t
  :config
  (keymap-unset js-mode-map "M-.")
  (keymap-unset js-ts-mode-map "M-.")
  (with-eval-after-load 'evil
    (evil-define-key 'normal js-mode-map
      (kbd "=") 'prettier-js))
  (add-hook 'js-mode-hook 'basic-ide))

;;; Java
(add-hook 'java-mode-hook (lambda()
                            (c-set-style "google")
                            (basic-ide)
                            (eglot-java-mode))
          30)
;;; Appearance and Aesthetics


;;;; Colourful Compilation
(require 'ansi-color)
(defun colorize-compilation ()
  "Colorize from `compilation-filter-start' to `point'."
  (let ((inhibit-read-only t))
    (ansi-color-apply-on-region
     compilation-filter-start (point))))
(add-hook 'compilation-filter-hook
          #'colorize-compilation)

;;;; Elcord
(use-package elcord
  :ensure t
  :defer t
  :custom
  (elcord-client-id
   (string-trim
    (get-string-from-file "~/Documents/emacs/elcord-id.txt")))
  (elcord-buffer-details-format-function
   (lambda ()
     (concat
      (format "Editing %s" (current-buffer))
      (when-let ((p (project-current)))
        (format " in %s" (file-name-nondirectory
                          (directory-file-name
                           (project-root p))))))))
  :config
  (nconc elcord-boring-buffers-regexp-list
         '("\\\\*scratch\\\\*" "\\\\*Help.*\\\\*" "\\\\*Man.*\\\\*" "\\\\*Customize.*\\\\*")))

;;;; Minimap
(use-package minimap
  :ensure t
  :defer t
  :custom
  (minimap-window-location 'right)
  (minimap-width-fraction 0.1))
;;;; highlight matching parens/brackets etc
(setq show-paren-delay 0.0)
(show-paren-mode 1)

;;;; Formatting
(setq-default indent-tabs-mode nil)
;; (add-hook 'before-save-hook 'whitespace-cleanup)
(use-package hl-indent
  :ensure t
  :hook
  ((python-mode haskell-mode) . hl-indent-mode))

;;; GUI features
(tool-bar-mode 0)
(scroll-bar-mode 0)
(setq tool-bar-style 'image)
(setopt tooltip-delay 0.02)
(window-divider-mode 1)
;; Like winner but better
(tab-bar-history-mode 1)

(setq-default indicate-empty-lines t)

;; Unbind menu key for context-menu-mode.
(assoc-delete-all 'menu context-menu-mode-map)
(context-menu-mode 1)

;; Window and Frame Size
(setq frame-resize-pixelwise t
      window-resize-pixelwise t
      window-combination-resize t)
(add-to-list 'default-frame-alist '(width . 90))

;;; TeX and LaTeX
;;;; General TeX
(use-package tex-mode
  :defer t
  :config
  (push '("zathura %r.pdf &" "%r.pdf") tex-compile-commands)
  (push '("latexmk -pdf -pvc %f" "%f") tex-compile-commands)
  (with-eval-after-load 'evil
    (evil-define-key 'normal tex-mode-map
      (kbd "<leader>c") 'tex-compile
      (kbd "<leader>SPC") 'tex-compile)))

(defun personal-tex-config ()
  (latex-electric-env-pair-mode)
  (electric-pair-local-mode 1)
  (outline-minor-mode)
  (basic-ide)
  (prettify-these
   '("\\pm" "±")
   '("\\sqrt" "√")
   '("\\land" "∧")
   '("\\lor" "∨")
   '("\\lnot" "¬")))
(add-hook 'latex-mode-hook 'personal-tex-config)

;;;; AucTeX
(use-package tex
  :ensure auctex
  :defer t
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-master t)
  :config
  (push '(output-pdf "PDF Tools") TeX-view-program-selection)
  (with-eval-after-load 'auctex
    (push '("lstlisting" current-indentation) LaTeX-indent-environment-list)
    (nconc LaTeX-verbatim-environments '("lstlisting" "minted")))
  (with-eval-after-load 'evil
    (evil-define-key 'normal TeX-mode-map
                     (kbd "<leader>SPC") 'TeX-command-master
                     (kbd "<leader>c") 'TeX-view))
  (defun personal-auctex-config ()
    (electric-pair-local-mode 1)
    (outline-minor-mode 1)
    (auto-fill-mode 1)
    (TeX-interactive-mode 1)
    (TeX-source-correlate-mode 1))

  (add-hook 'LaTeX-mode-hook 'personal-auctex-config)
  (add-to-list 'TeX-command-list
               '("Sage" "sage %s.sagetex.sage" TeX-run-command nil nil :help "Run Sage")
               t)
  (add-to-list 'TeX-command-list
               '("Latexmk" "latexmk -pvc -pdf %s" TeX-run-command nil nil :help "Run Latexmk")
               t))

;;;; LaTeX Table Wizard
(use-package latex-table-wizard
  :ensure t
  :after tex
  :config
  (setopt latex-table-wizard-new-environments-alist
          '(("longtable" :col
             ("&")
             :row
             ("\\\\" "\\endfirsthead" "\\endhead")
             :lines
             ("\\hline")))))

;;;; Evil TeX

(use-package evil-tex
  :ensure t
  :after (evil tex)
  :hook (LaTeX-mode . evil-tex-mode))

;;;; BibTeX

(use-package bibtex
  :defer t
  :custom
  (bibtex-dialect 'biblatex)
  (bibtex-autokey-titlewords 0)
  (bibtex-autokey-year-length 4)
  (bibtex-comment-start "%")
  :bind
  (:map bibtex-mode-map
        ("C-c ." . insert-isodate))
  :config
  (defun my-add-bibtex-entry-urldate ()
    "Set the urldate for a new entry to the current date, if necessary."
    (interactive)
    (require 'cl-lib)
    (save-mark-and-excursion
      (bibtex-beginning-of-entry)
      (when-let ((entry
                  (cl-loop for name in '("urldate" "OPTurldate" "ALTurldate")
                           for location = (bibtex-search-forward-field name t)
                           if location return location)))
        ;; Go to the opening brace of the entry text.
        (goto-char (cadr entry))
        (kill-sexp)
        (insert (format-time-string "{%Y-%m-%d}")))))
  (add-hook 'bibtex-add-entry-hook 'my-add-bibtex-entry-urldate)
  (push
   '(("urldate" . "\\([0-9]+\\)-\\([0-9]+\\)-\\([0-9]+\\)")
     "https://web.archive.org/web/%s/%s"
     ("urldate" "\\([0-9]+\\)-\\([0-9]+\\)-\\([0-9]+\\)" "\\1\\2\\3")
     ("url" ".*:.*" 0))
   bibtex-generate-url-list)
  (add-hook 'bibtex-mode-hook
            (lambda ()
              (outline-minor-mode 1)
              (corfu-mode 1))))

;;;; RefTeX
(with-eval-after-load 'evil
  (evil-set-initial-state 'reftex-select-bib-mode 'emacs))
(with-eval-after-load 'org
  (setq reftex-default-bibliography org-cite-global-bibliography))

;;; Groff
(use-package mom-mode
  :ensure t
  :defer t
  :config
  (add-to-list 'auto-mode-alist '("\\.mom" . mom-mode))
  (add-hook 'mom-mode-hook 'outline-minor-mode))
(add-to-list 'auto-mode-alist '("\\.tmac\\'" . nroff-mode))
;;; PDF Tools
(use-package pdf-tools
  :ensure t
  :bind
  (:map pdf-view-mode-map
        ("s a" . pdf-view-auto-slice-minor-mode))
  :config
  (pdf-tools-install)
  (defun personal-pdf-view-config ()
    (auto-revert-mode)
    (setq display-line-numbers nil))
  (add-hook 'pdf-view-mode-hook 'personal-pdf-view-config 50))

(use-package org-pdftools
  :ensure t
  :after (org pdf-tools)
  :hook (org-mode . org-pdftools-setup-link))
(use-package org-noter-pdftools
  :ensure t
  :after org-noter)
;;; Google C Style
(use-package google-c-style
  :ensure t
  :after (:any cc-mode c-ts-mode)
  :config
  (c-add-style "Google" google-c-style))

;;; Editorconfig
(use-package editorconfig
  :ensure t
  :delight editorconfig-mode)
(editorconfig-mode 1)

;;; Repeat Mode
(repeat-mode 1)
(setopt repeat-keep-prefix t)
(defvar-keymap
    drag-repeat-map
  :repeat t
  "n" 'org-drag-line-forward
  "p" 'org-drag-line-backward)
(keymap-global-set "C-c n" 'org-drag-line-forward)
(keymap-global-set "C-c p" 'org-drag-line-backward)
(defvar-keymap
    increment-repeat-map
  :repeat t
  "+" 'org-increase-number-at-point
  "-" 'org-decrease-number-at-point)
(keymap-global-set "C-c +" 'org-increase-number-at-point)
(keymap-global-set "C-c -" 'org-decrease-number-at-point)
(keymap-global-set "C-c C-+" 'org-increase-number-at-point)
(keymap-global-set "C-c C--" 'org-decrease-number-at-point)
(defvar-keymap
    sexp-repeat-map
  :repeat t
  "<left>" 'backward-sexp
  "<right>" 'forward-sexp
  "<down>" 'down-list
  "<up>" 'backward-up-list)
(bind-key "g" sexp-repeat-map ctl-x-map)
(setopt windmove-create-window t)
(windmove-default-keybindings '(super control))
(windmove-swap-states-default-keybindings '(super meta))
(defvar-keymap
    switch-buffer-repeat-map
  :repeat t
  "<left>" 'previous-buffer
  "<right>" 'next-buffer)

(defvar-keymap
    horizontal-scroll-repeat-map
  :repeat t
  "<" 'scroll-left
  ">" 'scroll-right)

(defvar-keymap
    smerge-next-repeat-map
  :repeat t
  "n" 'smerge-next
  "p" 'smerge-prev)

(defvar-keymap
    re-builder-match-navigation-repeat-map
  :repeat t
  "C-s" 'reb-next-match
  "C-r" 'reb-prev-match)

(defvar-keymap
    gnus-summary-put-mark-as-unread-repeat-map
  :repeat t
  "N" 'gnus-summary-put-mark-as-unread-next-unread
  "P" 'gnus-summary-put-mark-as-unread-prev-unread
  "n" 'gnus-summary-put-mark-as-unread-next
  "p" 'gnus-summary-put-mark-as-unread-prev
  "u" 'gnus-summary-put-mark-as-unread)

;;; Precise Pixel Scroll
(if (version<= "29.0" emacs-version)
    (pixel-scroll-precision-mode 1))

;;; Find File (at Point)
;; We want access to all the cool ffap commands.
(require 'find-file)
(require 'ffap)

(add-to-list ff-other-file-alist '("\\.svelte\\'" (".server.js" ".server.ts")))
(add-to-list ff-other-file-alist '("\\.server.ts\\'" (".svelte")))
(add-to-list ff-other-file-alist '("\\.server.js\\'" (".svelte")))

;;; Performance
;; Adjust thresholds for the 21st Century
(setq gc-cons-threshold 100000000)
(setq read-process-output-max (* 1024 1024))
(setq large-file-warning-threshold 100000000)

;;; Snippets/Insertion Macros

(require 'tempo)


;; Auto-insert mode: A load of built-in templates for many modes.
(auto-insert-mode 1)

;; Expand: A super minimal option that does enough a lot of the time.
;; TODO: Put these in a minor mode, transient binding or something.  This is dumb.
(add-hook 'expand-expand-hook
          (lambda ()
            (indent-according-to-mode)
            (when (< expand-index (- (length expand-pos) 1))
              (local-set-key (kbd "TAB") 'expand-jump-to-next-slot))))
(add-hook 'expand-jump-hook
          (lambda ()
            (indent-according-to-mode)
            (when (= expand-index (- (length expand-pos) 1))
              (local-unset-key (kbd "TAB")))))

(require 'skeleton)
(define-skeleton skeleton-c-main
  "Insert the main function for C and C++."
  ""
  "int main("
  (when (y-or-n-p "Insert argc & argv?")
    "int argc, char *argv[]")
  ") {" \n
  > _ \n
  "}" >)
(define-skeleton skeleton-c-for
  "Insert a for loop.  Not actually useful, really."
  "Variable: "
  "for (" (read-string "Type: ") & " "
  str
  " = " (read-string "Initialiser: ") | -3
  "; "
  str
  " " (read-string "Loop condition: ") | -1
  "; " str
  (read-string "Increment (default ++): " nil nil "++")
  ") {"
  _
  "}")

(defun insert-isodate ()
  "Insert a date in ISO format."
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))

(use-package cmake-ts-mode
  :ensure t)
(define-skeleton skeleton-cmake-autoinsert
  "Autoinsert for CMake files."
  "Project Name: "
  _
  "cmake_minimum_required(VERSION "
  (read-string "Minimum Version: " nil nil "3.15")
  ")" \n \n

  "project(" str " VERSION 0.1)" \n
  '(setq export-compile-commands (y-or-n-p "Export Compile Commands?"))
  (when export-compile-commands
    "set(CMAKE_EXPORT_COMPILE_COMMANDS True)\n\n")

  "set(CMAKE_"
  (let* ((language
          (read-multiple-choice "Language?" '((?c "C") (?x "CXX"))))
         (standard
          (read-string (concat (cadr language) " standard: ") nil nil
                       (case (car language)
                         (?c "17")
                         (?x "20")))))
    (concat
     (cadr language) "_STANDARD " standard ")\n"
     "set(CMAKE_" (cadr language) "_STANDARD_REQUIRED "
     (if (y-or-n-p "Required?") "True" "False")
     ")\n\n"))

  "add_executable(" str " "
  '(setq
    src-dir (file-name-as-directory (file-relative-name (read-directory-name "Source Directory: ")))
    abs-src-dir (file-name-concat default-directory src-dir))
  ((let ((src-file
          (file-relative-name
           (read-file-name
            "Source File: "
            abs-src-dir
            abs-src-dir
            nil
            nil))))
     (unless (string= src-file src-dir)
       src-file))
   str & " ")
  ;; Remove final space. Shouldn't be necessary?!
  -1
  ")" \n \n

  ((let ((subdir-name
          (file-relative-name (read-directory-name "Subdirectory: "))))
     (unless (string= subdir-name "./")
       subdir-name))
   "add_subdirectory(" str & ")" & \n | -17)
  \n

  '(setq cmake-project-name str)
  ((let ((choice
          (read-multiple-choice "Scope: " '((?u "public") (?r "private") (?i "interface")
                                            (?q "quit")))))
     (unless (eq (car choice) '?q)
       (upcase (cadr choice))))

   "target_include_directories(" cmake-project-name " "
   str
   " "
   ((let ((includedir-name
           (file-relative-name (read-directory-name "Include Directory: "))))
      (unless (string= includedir-name "./")
        includedir-name))
    str & " ")
   -1 ")" \n)
  \n

  ("Package %s: "
   "find_package(" str & " REQUIRED)" & \n | -13)
  \n

  ((let ((choice
          (read-multiple-choice "Scope: " '((?u "public") (?r "private") (?i "interface")
                                            (?q "quit")))))
     (unless (eq (car choice) '?q)
       (upcase (cadr choice))))

   "target_link_libraries(" cmake-project-name " "
   str
   " "
   ("Library %s: "
    str & " ")
   -1 ")" \n)
  \n

  "target_compile_options(" str " PRIVATE" \n
  "-Wall" > \n
  "-Wextra" > \n
  "-Wimplicit-fallthrough" > \n
  "$<$<CONFIG:Release>:-Werror>" > \n
  "$<$<CONFIG:Debug>:-g3>" > \n
  "$<$<CONFIG:Debug>:-Og>" > \n
  "$<$<CONFIG:Release>:-O3>" > \n
  "-g" > \n
  ")" > \n \n


  (when export-compile-commands
    "file(CREATE_LINK \"${CMAKE_BINARY_DIR}/compile_commands.json\" \"${CMAKE_SOURCE_DIR}/compile_commands.json\" SYMBOLIC)"))

(delight 'abbrev-mode " 🚀" 'abbrev)

;;; General Prose
(defun prose-config ()
  "Configuration for writing prose."
  (flyspell-mode)
  (delight 'flyspell-mode "" 'flyspell)
  (abbrev-mode))
(bind-key "H-t" 'transpose-sentences text-mode-map)
(bind-key "C-c t" 'transpose-paragraphs text-mode-map)
(bind-key "C-c M-t" 'transpose-sentences text-mode-map)
(add-hook 'text-mode-hook 'prose-config)
(use-package flyspell
  :defer t
  :bind
  (:map flyspell-mode-map
        ("H-<f7>" . count-words)
        ("H-<f8>" . flyspell-buffer)))

;; Bri'ish
(setq ispell-dictionary "en_GB")


;;; FlyMake
;;;; Keybindings
(use-package flymake
  :defer t
  :bind
  (:map flymake-mode-map
        ("M-n" . flymake-goto-next-error)
        ("M-p" . flymake-goto-prev-error)
        ("<f8>" . flymake-show-buffer-diagnostics)
        ("M-<f8>" . flymake-show-project-diagnostics)))
(bind-key "C-<f8>" 'next-error-select-buffer)

;;; Undo Tree (necessary for evil-undo-system)
(use-package vundo
  :ensure t
  :bind
  ("H-/" . vundo)
  :config
  (setq vundo-glyph-alist vundo-unicode-symbols))
;;; line and column numbers
(column-number-mode 1)
(line-number-mode 1)

;;; automatic pairs
(add-hook 'prog-mode-hook 'electric-pair-local-mode)
(add-hook 'minibuffer-mode-hook 'electric-pair-local-mode)

(if (version< emacs-version "29.0")
    (add-hook 'sgml-mode-hook
              (lambda ()
                (electric-pair-local-mode 0))))
(defun my-insert-pair (&optional arg c)
  "Like `insert-pair', but cooler.
Handles ARG as `insert-pair' does, interactively reading prompt character C.
The closing character is looked up in `insert-pair-alist' using `assoc'.
If no character is found there, C is used as the closing character as well."
  (interactive "P\ncPair character:")
  (insert-pair arg c (or (cadr (assoc c insert-pair-alist)) c)))
(bind-key "C-'" 'my-insert-pair)
(bind-key "C-#" 'delete-pair)
(setq delete-pair-blink-delay 0)
;;; code folding
;;;; Hideshow
(use-package hideshow
  :hook (prog-mode . hs-minor-mode)
  :delight hs-minor-mode
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal hs-minor-mode-map
      (kbd "zy") 'hs-hide-level
      (kbd "zi") 'hs-toggle-hiding)))
;;;; Outline
(use-package outline
  :defer t
  :bind
  (:map outline-minor-mode-map
        ("H-<up>" . outline-move-subtree-up)
        ("H-<down>" . outline-move-subtree-down)
        ("H-<right>" . outline-demote)
        ("H-<left>" . outline-promote))
  :config
  (with-eval-after-load 'evil
    (evil-define-key 'normal outline-minor-mode-map
      (kbd "zy") 'outline-hide-sublevels
      (kbd "zs") 'outline-show-subtree
      (kbd "zj") 'outline-hide-other
      (kbd "S-<up>") 'outline-move-subtree-up
      (kbd "S-<down>") 'outline-move-subtree-down
      (kbd "S-<left>") 'outline-promote
      (kbd "S-<right>") 'outline-demote
      (kbd "<leader>O") 'consult-outline)))
(delight 'outline-minor-mode "" 'outline)

;;; Elpher (Gopher and Gemini)
(use-package elpher
  :ensure t)
(add-hook 'elpher-mode-hook
          (lambda()
            (variable-pitch-mode 1)
            (text-scale-set 1)))

;;; Mail and News
(use-package gnus
  :ensure t
  :config
  (setq gnus-startup-file (file-name-concat gnus-directory "newsrc"))
  (setq
   user-full-name "Aidan Hall"
   user-mail-address "aidan.hall202@gmail.com"
   mm-decrypt-option 'known
   mm-discouraged-alternatives '("image/.*" "text/html")

   gnus-select-method
   '(nnimap "gmail"
            (nnimap-address "imap.gmail.com")
            (nnimap-server-port "imaps")
            (nnimap-stream ssl))

   gnus-secondary-select-methods
   '((nntp "news.gmane.io"))
   gnus-asynchronous t
   gnus-use-toolbar 'default
   gnus-group-tool-bar 'gnus-group-tool-bar-gnome

   smtpmail-smtp-server "smtp.gmail.com"
   smtpmail-smtp-service 587
   gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]"
   gnus-use-long-file-name nil
   gnus-treat-display-smileys nil
   message-user-organization "Black Mesa")
  (add-hook 'gnus-group-mode-hook 'gnus-topic-mode)
  (with-eval-after-load 'evil
    (evil-set-initial-state 'gnus-mode 'emacs)
    (evil-set-initial-state 'gnus-group-mode 'emacs)
    (evil-set-initial-state 'gnus-article-mode 'emacs)
    (evil-set-initial-state 'gnus-summary-mode 'emacs)
    (evil-set-initial-state 'gnus-server-mode 'emacs)
    (evil-set-initial-state 'gnus-browse-mode 'emacs)
    (evil-set-initial-state 'gnus-tree-mode 'emacs)
    (evil-set-initial-state 'gnus-score-mode 'emacs)))

;;; IRC
(setq erc-nick "Argletrough")

;;; line numbers
(setq display-line-numbers-type 'visual)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'text-mode-hook 'display-line-numbers-mode)

;;; Fill Column and Wrapping
(global-display-fill-column-indicator-mode 1)
(setq vc-git-log-edit-summary-target-len 50)

;;; Visual Line Mode
(setq visual-line-fringe-indicators '(left-curly-arrow right-curly-arrow))

;;; Comfortable Behaviour

;;;; WYSIWYG

(define-minor-mode wysiwyg-mode
  "Adjusts some settings to enable WYSIWYG/GUI-style editing."
  :lighter (" 📄" wysiwyg-mode-lighter)
  :init-value nil
  (variable-pitch-mode (if wysiwyg-mode 1 -1))
  (if wysiwyg-mode
      (display-line-numbers-mode -1)
    (display-line-numbers-mode 1))
  (visual-line-mode (if wysiwyg-mode 1 -1))
  (text-scale-set (if wysiwyg-mode 1 0)))

(define-minor-mode org-notes-mode
  "Set up an `org-mode' buffer for taking notes."
  :lighter (" ✍️" org-notes-mode-lighter)
  :init-value nil
  (wysiwyg-mode (if org-notes-mode 1 -1))
  (org-indent-mode (if org-notes-mode 1 -1)))

;;;; Fixing Broken Default Settings
;; disable audible bell
(setq visible-bell 1)

;; Y or N instead of Yes or No
(setopt use-short-answers t)

;; Global auto-revert
(setq global-auto-revert-non-file-buffers t)
(global-auto-revert-mode 1)

;;;; Activity History/Persistence
;; Recent Files
(recentf-mode 1)
(add-hook 'buffer-list-update-hook 'recentf-track-opened-file)
(setq recentf-max-saved-items 1000)

;; Recursive minibuffer
(define-minor-mode recursive-minibuffer-mode
  "Enables recursive minibuffers with depth indication."
  :init-value nil
  :global t
  (setq enable-recursive-minibuffers recursive-minibuffer-mode)
  (minibuffer-depth-indicate-mode recursive-minibuffer-mode))

;; Minibuffer History
(setq history-length 1000)
(savehist-mode 1)

;; Saving Place
(save-place-mode 1)
;; Desktop
(use-package desktop
  :custom
  (desktop-restore-eager 5)
  (desktop-restore-in-current-display nil)
  (desktop-restore-forces-onscreen nil)
  (desktop-restore-frames nil)
  (desktop-modes-not-to-save
   '(sfeed-data-file-mode tags-table-mode)))
(desktop-save-mode 1)

;;;; Removed safety
(put 'narrow-to-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'narrow-to-page 'disabled nil)

;;; Dragon
;; Open a Dragon window of the current buffer's file.
(defun super-buffer-file (&optional buffer-or-name)
  "Name of a local file containing buffer `BUFFER-OR-NAME''s contents.
Uses `file-local-copy' and `make-temp-file' if necessary to make a file to use.
Uses `current-buffer' by default."
  (interactive)
  (let* ((buf (get-buffer (or buffer-or-name
                              (current-buffer))))
         (name (buffer-name buf)))
    (if buf
        (with-current-buffer buf
          (or (and (buffer-file-name)
                   (or (file-local-copy (buffer-file-name))
                       (file-local-name (buffer-file-name))))
              (let ((tmp-file (make-temp-file (buffer-name))))
                (write-file tmp-file)
                tmp-file)))
      buffer-or-name)))

(defun dragon-buffer (&optional buffer-or-file)
  "Call dragon on `BUFFER-OR-FILE'.
If unspecified, `dired-get-marked-files' is used.
If that is unspecified (not a Dired buffer), `current-buffer' is used.
Uses `super-buffer-file' to get a file for a non-Dired buffer."
  (interactive)
  (set-process-sentinel
   (apply 'start-process "Dragon"
          (concat "*Dragon: " (buffer-name) "*")
          "dragon"
          "--and-exit"
          "--all"
          (or (and (null buffer-or-file) (dired-get-marked-files))
              (list (super-buffer-file buffer-or-file))))
   `(lambda (process signal)
      (and (memq (process-status process) '(exit signal))
           (buffer-live-p (process-buffer process))
           (kill-buffer (process-buffer process))))))
(bind-key "C-<f1>" 'dragon-buffer)

(require 'notifications)
(add-hook 'emacs-startup-hook (lambda ()
                                (notifications-notify :title "Emacs Ready" :body (emacs-init-time))))

;; Use pass as an auth-source
(with-eval-after-load 'auth-source
  (auth-source-pass-enable))
;; Periodic eyestrain break
(run-at-time (* 20 60) (* 20 60)
             'notifications-notify
             :title "Eyestrain Break"
             :body "Stare at infinity for 20 seconds.")
(load custom-file)
(provide 'init)
;;; init.el ends here
;; This is broken, but the body does what we want.
;; (defadvice casual-avy-imenu-support-p (around do-it-correctly activate)
;;   "This actually detects imenu support correctly."
;;   (ignore-error 'imenu-unavailable (imenu--make-index-alist)))
