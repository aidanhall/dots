(setq org-latex-hyperref-template
      "\\hypersetup{
 pdfauthor={%a},
 pdftitle={%t},
 pdfkeywords={%k},
 pdfsubject={%d},
 pdfcreator={%c}, 
 pdflang={%L},
 colorlinks=true} ")

(setq org-publish-project-alist
      '(("Roam" :base-directory "~/Documents/org/roam/" :publishing-directory "~/Documents/org/roam-html/")
        ("zettelkasten basic" :base-directory "~/Documents/emacs/zettelkasten/" :publishing-directory "~/Documents/emacs/published-zettelkasten/")))
(setq org-latex-compiler "xelatex"
      org-latex-reference-command "\\autoref{%s}"
      org-src-preserve-indentation t
      org-directory "~/Documents/org"
      org-roam-directory (file-name-concat org-directory "roam")
      org-list-allow-alphabetical t
      org-cite-global-bibliography '("~/Documents/latex/bibliography.bib")
      org-export-default-language "en-gb"
      org-latex-inputenc-alist '(("utf8" . "utf8x"))
      org-preview-latex-process-alist
      '((dvipng :programs
                ("latex" "dvipng")
                :description "dvi > png" :message "you need to install the programs: latex and dvipng." :image-input-type "dvi" :image-output-type "png" :image-size-adjust
                (2.0 . 2.0)
                :latex-compiler
                ("latex -interaction nonstopmode -output-directory %o %f")
                :image-converter
                ("dvipng -D %D -T tight -o %O %f")
                :transparent-image-converter
                ("dvipng -D %D -T tight -bg Transparent -o %O %f"))
        (dvisvgm :programs
                 ("latex" "dvisvgm")
                 :description "dvi > svg" :message "you need to install the programs: latex and dvisvgm." :image-input-type "dvi" :image-output-type "svg" :image-size-adjust
                 (2.0 2.0)
                 :latex-compiler
                 ("latex -interaction nonstopmode -output-directory %o %f")
                 :image-converter
                 ("dvisvgm %f --no-fonts --exact-bbox --scale=%S --output=%O"))
        (imagemagick :programs
                     ("latex" "convert")
                     :description "pdf > png" :message "you need to install the programs: latex and imagemagick." :image-input-type "pdf" :image-output-type "png" :image-size-adjust
                     (1.0 . 1.0)
                     :latex-compiler
                     ("pdflatex -interaction nonstopmode -output-directory %o %f")
                     :image-converter
                     ("convert -density %D -trim -antialias %f -quality 100 %O")))
      org-latex-default-packages-alist
      '(("AUTO" "inputenc" t
         ("pdflatex"))
        ("T1" "fontenc" t
         ("pdflatex"))
        ("" "graphicx" t nil)
        ("" "longtable" nil nil)
        ("" "wrapfig" nil nil)
        ("" "rotating" nil nil)
        ("normalem" "ulem" t nil)
        ("" "mathtools" t nil)
        ("" "amssymb" t nil)
        ("" "capt-of" nil nil)
        ("" "hyperref" nil nil))
      org-link-abbrev-alist
      '(("lib" . "https://warwick.summon.serialssolutions.com/search?q=")
	("m" . "https://go.warwick.ac.uk/")
	("doi" . "https://doi.org/")))

(require 'ox-beamer)
(require 'ox-md)
(require 'ox-html)


(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (R . t)
   (lua . t)
   (python . t)
   (lisp . t)
   (calc . t)
   (gnuplot . t)
   (awk . t)
   (haskell . t)))

(provide 'org-export-conf)
;;; org-export-conf.el ends here
