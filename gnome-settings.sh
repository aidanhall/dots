#!/usr/bin/env sh
CONFIGS=$(find gnome -type f)
ACTION="$1"
for FILE in $CONFIGS; do
    F="/$(basename $FILE | sed 's/\./\//g')"
    case $ACTION in
        load)
            echo "Loading settings for $F"
            if [ ${F: -1} = "/" ]; then
                dconf load "$F" < $FILE
            else
                dconf write "$F" "$(cat $FILE)"
            fi
            ;;
        dump)
            echo "Dumping settings for $F"
            if [ ${F: -1} = "/" ]; then
                dconf dump "$F" > $FILE
            else
                dconf read "$F" > $FILE
            fi
            ;;
        *)
            echo "Usage: $0 [load|dump]"
            exit 1
    esac
done
