;; This is an operating system configuration generated
;; by the graphical installer.
;;
;; Once installation is complete, you can learn and modify
;; this file to tweak the system configuration, and pass it
;; to the 'guix system reconfigure' command to effect your
;; changes.


;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu)
             (gnu packages shells)
             (gnu packages xorg)
             (gnu packages video)
             (gnu packages gnome)
             (gnu services pm)
             (gnu services desktop)
             (gnu services xorg)
             (nongnu packages linux)
             (nongnu packages video)
             (nongnu packages firmware)
             (nongnu packages nvidia)
             (nongnu services nvidia)
             (guix packages)
             (guix channels)
             (gnu packages emacs-xyz)
             (guix utils))
(use-service-modules cups desktop networking ssh xorg sound)

(load "channels.scm")

(operating-system
 (locale "en_GB.utf8")
 (timezone "Europe/London")
 (keyboard-layout (keyboard-layout "gb"))
 (host-name "guix-pc")

 (kernel linux)
 (kernel-arguments '("modprobe.blacklist=nouveau"
                     ;; Set this if the card is not used for displaying or
                     ;; you're using Wayland:
                     "nvidia_drm.modeset=1"))
 (firmware (append (list iwlwifi-firmware)
                   %base-firmware))

 ;; The list of user accounts ('root' is implicit).
 (users (cons* (user-account
                (name "aidan")
                (comment "Aidan Hall")
                (group "users")
                (home-directory "/home/aidan")
                (supplementary-groups '("wheel" "netdev" "audio" "video"))
                )
               %base-user-accounts))

 ;; Packages installed system-wide.  Users can also install packages
 ;; under their own account: use 'guix search KEYWORD' to search
 ;; for packages and 'guix install PACKAGE' to install a package.
 (packages (append
            (specifications->packages
             (list
              "emacs"
              "playerctl"
              "iwlwifi-firmware"
              "hicolor-icon-theme"
              "font-adobe-source-code-pro"
              "font-google-noto"
              "font-google-noto-emoji"
              "font-google-noto-sans-cjk"
              "font-google-noto-serif-cjk"
              "man-db"
              "gnupg"
              "icedove"
              "firefox"
              "tmux"))
            %base-packages))

 ;; Below is the list of system services.  To search for available
 ;; services, run 'guix system search KEYWORD' in a terminal.
 (services
  (modify-services
   (append (list

            ;; To configure OpenSSH, pass an 'openssh-configuration'
            ;; record as a second argument to 'service' below.
            (service openssh-service-type)
            (service bluetooth-service-type)
            (service gnome-desktop-service-type
                     ;; May be necessary for nvidia graphics,
                     ;; see the nonguix readme.
                     ;; (gnome-desktop-configuration
                     ;;  (gnome (replace-mesa gnome)))
                     )
            (service tlp-service-type
                     (tlp-configuration
                      (cpu-scaling-governor-on-bat (list "powersave"))))
            (set-xorg-configuration
             (xorg-configuration
              (keyboard-layout keyboard-layout)
              (modules (cons nvda %default-xorg-modules))
              (drivers '("nvidia")))))

           ;; This is the default list of services we
           ;; are appending to.
           %desktop-services)
   (gdm-service-type
    config =>
    (gdm-configuration
     (wayland? #t)))
   (guix-service-type
    config =>
    (guix-configuration
     (inherit config)
     (guix (guix-for-channels %channels))
     (channels %channels)
     (substitute-urls
      (append %default-substitute-urls
              (list "https://substitutes.nonguix.org")))
     (authorized-keys
      (append %default-authorized-guix-keys
              (list (plain-file "non-guix.pub"
                                "(public-key
 (ecc
  (curve Ed25519)
  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
  )
 )")))))))
  )
 (bootloader (bootloader-configuration
              (bootloader grub-bootloader)
              (targets (list "/dev/sdb"))
              (keyboard-layout keyboard-layout)))

 ;; The list of file systems that get "mounted".  The unique
 ;; file system identifiers there ("UUIDs") can be obtained
 ;; by running 'blkid' in a terminal.
 (file-systems (cons* (file-system
                       (mount-point "/")
                       (device (file-system-label "guix-root"))
                       (type "ext4")) %base-file-systems)))
